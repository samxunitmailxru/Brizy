const Advisors = require("./Advisors");
const AlpineLodge = require("./AlpineLodge");
const Architekt = require("./Architekt");
const BaseGround = require("./BaseGround");
const BeachResort = require("./BeachResort");
const CarClinic = require("./CarClinic");
const College = require("./College");
const Creed = require("./Creed");
const Flavour = require("./Flavour");
const Gourmet = require("./Gourmet");
const Hitched = require("./Hitched");
const Hope = require("./Hope");
const InShape = require("./InShape");
const Keynote = require("./Keynote");
const KidQuest = require("./KidQuest");
const Lavish = require("./Lavish");
const Molino = require("./Molino");
const Moves = require("./Moves");
const Parlor = require("./Parlor");
const Philanthropy = require("./Philanthropy");
const Quantum = require("./Quantum");
const ReelStory = require("./ReelStory");
const Skypoint = require("./Skypoint");
const Startapp = require("./Startapp");
const Swipe = require("./Swipe");
const Wellness = require("./Wellness");
const Yoga = require("./Yoga");

module.exports = {
  templates: [
    Advisors,
    AlpineLodge,
    Architekt,
    BaseGround,
    BeachResort,
    CarClinic,
    College,
    Creed,
    Flavour,
    Gourmet,
    Hitched,
    Hope,
    InShape,
    Keynote,
    KidQuest,
    Lavish,
    Molino,
    Moves,
    Parlor,
    Philanthropy,
    Quantum,
    ReelStory,
    Skypoint,
    Startapp,
    Swipe,
    Wellness,
    Yoga
  ],
  categories: [
    { id: 1, title: "Business" },
    { id: 2, title: "Travel" },
    { id: 3, title: "Portfolio" },
    { id: 4, title: "Real-Estate" },
    { id: 5, title: "Automotive" },
    { id: 6, title: "Education" },
    { id: 7, title: "Religion" },
    { id: 8, title: "Events" },
    { id: 10, title: "Food" },
    { id: 11, title: "One Page" },
    { id: 12, title: "Non Profit" },
    { id: 13, title: "Health & Beauty" },
    { id: 14, title: "Sport" }
  ]
};
