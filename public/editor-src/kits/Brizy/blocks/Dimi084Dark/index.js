module.exports = {
id: "Dimi084Dark",

thumbnailWidth: 600,
thumbnailHeight: 80,
title: "Dimi084Dark",

keywords: "Header",
cat: [21],
type: 1,
pro: true,
resolve: {
    type: "SectionHeader",
    value: {
        _styles: [
            "sectionHeader"
        ],
        items: [
            {
                type: "SectionHeaderItem",
                value: {
                    _styles: [
                        "sectionHeaderItem"
                    ],
                    items: [
                        {
                            type: "Row",
                            value: {
                                _styles: [
                                    "row"
                                ],
                                items: [
                                    {
                                        type: "Column",
                                        value: {
                                            _styles: [
                                                "column"
                                            ],
                                            items: [
                                                {
                                                    type: "Wrapper",
                                                    value: {
                                                        _styles: [
                                                            "wrapper",
                                                            "wrapper--image"
                                                        ],
                                                        items: [
                                                            {
                                                                type: "Image",
                                                                value: {
                                                                    _styles: [
                                                                        "image"
                                                                    ],
                                                                    imagePopulation: "",
                                                                    imageWidth: 2129,
                                                                    imageHeight: 457,
                                                                    imageSrc: "0785e166b9764f9ec5065372c0ddc1c4.png",
                                                                    height: 100,
                                                                    positionX: 50,
                                                                    positionY: 50,
                                                                    resize: 45,
                                                                    mobileResize: 40,
                                                                    tabletResize: 70
                                                                }
                                                            }
                                                        ],
                                                        horizontalAlign: "left",
                                                        mobileHorizontalAlign: "center"
                                                    }
                                                }
                                            ],
                                            verticalAlign: "center",
                                            width: 33.2999999999999971578290569595992565155029296875,
                                            tabletWidth: 33.39999999999999857891452847979962825775146484375,
                                            paddingTop: 0,
                                            paddingTopSuffix: "px",
                                            padding: 15,
                                            paddingBottom: 0,
                                            paddingBottomSuffix: "px"
                                        }
                                    },
                                    {
                                        type: "Column",
                                        value: {
                                            _styles: [
                                                "column"
                                            ],
                                            items: [
                                                {
                                                    type: "Wrapper",
                                                    value: {
                                                        _styles: [
                                                            "wrapper",
                                                            "wrapper--menu"
                                                        ],
                                                        items: [
                                                            {
                                                                type: "Menu",
                                                                value: {
                                                                    _styles: [
                                                                        "menu"
                                                                    ],
                                                                    menuSelected: "2f2a438df28967e236f9b36c36d7b9ed",
                                                                    fontSize: 19,
                                                                    fontFamily: "josefin_sans",
                                                                    lineHeight: 1.6999999999999999555910790149937383830547332763671875,
                                                                    letterSpacing: 0,
                                                                    fontWeight: 400,
                                                                    fontStyle: "",
                                                                    itemPadding: 45,
                                                                    itemPaddingRight: 45,
                                                                    itemPaddingLeft: 45,
                                                                    horizontalAlign: "center",
                                                                    colorHex: "#ffffff",
                                                                    colorOpacity: 1,
                                                                    colorPalette: "",
                                                                    tempColorOpacity: 1,
                                                                    hoverColorHex: "#ffffff",
                                                                    hoverColorOpacity: 1,
                                                                    hoverColorPalette: "color3",
                                                                    subMenuBgColorPalette: "",
                                                                    subMenuBgColorOpacity: 1,
                                                                    subMenuHoverBgColorPalette: "",
                                                                    mMenuBgColorPalette: "",
                                                                    subMenuBorderColorHex: "#000000",
                                                                    subMenuBorderColorOpacity: 0.08000000000000000166533453693773481063544750213623046875,
                                                                    subMenuBorderColorPalette: "",
                                                                    subMenuTempBorderColorOpacity: 0.08000000000000000166533453693773481063544750213623046875,
                                                                    subMenuHoverBorderColorHex: "#000000",
                                                                    subMenuHoverBorderColorOpacity: 0.070000000000000006661338147750939242541790008544921875,
                                                                    mMenuBorderColorHex: "#000000",
                                                                    mMenuBorderColorOpacity: 0.08000000000000000166533453693773481063544750213623046875,
                                                                    subMenuColorHex: "#191b21",
                                                                    subMenuColorOpacity: 1,
                                                                    subMenuColorPalette: "color1",
                                                                    subMenuTempColorOpacity: 1,
                                                                    subMenuHoverColorHex: "#191b21",
                                                                    subMenuHoverColorOpacity: 1,
                                                                    mMenuColorHex: "#191b21",
                                                                    mMenuColorOpacity: 1,
                                                                    mMenuHoverColorHex: "#191b21",
                                                                    mMenuHoverColorOpacity: 1,
                                                                    subMenuHoverColorPalette: "color3",
                                                                    mMenuHoverColorPalette: "color3",
                                                                    subMenuHoverBorderColorPalette: "",
                                                                    subMenuTempHoverBorderColorOpacity: 0.070000000000000006661338147750939242541790008544921875,
                                                                    subMenuHoverBgColorHex: "#f9f9f9",
                                                                    subMenuHoverBgColorOpacity: 1,
                                                                    tabletMMenuSize: 25,
                                                                    tabletHorizontalAlign: "center",
                                                                    tabletMMenuFontSize: 18,
                                                                    tabletMMenuLineHeight: 2.5,
                                                                    tabletMMenuLetterSpacing: 0,
                                                                    tabletMMenuFontWeight: 600,
                                                                    tabletMMenuFontStyle: "",
                                                                    mMenuBgColorHex: "#ffffff",
                                                                    mMenuBgColorOpacity: 1,
                                                                    mMenuTempBgColorOpacity: 1,
                                                                    mMenuBorderColorPalette: "",
                                                                    mMenuTempBorderColorOpacity: 0.08000000000000000166533453693773481063544750213623046875,
                                                                    mMenuTitle: "Main Menu",
                                                                    mobileHorizontalAlign: "center",
                                                                    mobileMMenuSize: 25,
                                                                    mMenuColorPalette: "color1",
                                                                    mMenuTempColorOpacity: 1,
                                                                    mobileMMenuFontSize: 18,
                                                                    mobileMMenuLineHeight: 2.5,
                                                                    mobileMMenuLetterSpacing: 0,
                                                                    mobileMMenuFontWeight: 600,
                                                                    mobileMMenuFontStyle: "",
                                                                    tabletMMenuIconColorHex: "#ffffff",
                                                                    tabletMMenuIconColorOpacity: 1,
                                                                    tabletMMenuIconColorPalette: "",
                                                                    mMenu: "on",
                                                                    mMenuSize: 25,
                                                                    subMenuBgColorHex: "#ffffff",
                                                                    subMenuTempBgColorOpacity: 1,
                                                                    subMenuFontSize: 17,
                                                                    subMenuFontFamily: "josefin_sans",
                                                                    subMenuLineHeight: 1,
                                                                    subMenuLetterSpacing: 0,
                                                                    subMenuFontWeight: 600,
                                                                    subMenuFontStyle: "",
                                                                    mMenuIconColorPalette: "",
                                                                    mMenuIconColorHex: "#ffffff",
                                                                    mMenuIconColorOpacity: 1,
                                                                    mMenuFontSize: 18,
                                                                    mMenuFontFamily: "josefin_sans",
                                                                    mMenuLineHeight: 2.5,
                                                                    mMenuLetterSpacing: 0,
                                                                    mMenuFontWeight: 600,
                                                                    mMenuFontStyle: "",
                                                                    tabletMMenuItemHorizontalAlign: "center",
                                                                    mobileMMenuItemHorizontalAlign: "center",
                                                                    mMenuItemHorizontalAlign: "center",
                                                                    tabletMMenu: "on"
                                                                }
                                                            }
                                                        ],
                                                        showOnTablet: "on"
                                                    }
                                                }
                                            ],
                                            verticalAlign: "center",
                                            width: 33.2999999999999971578290569595992565155029296875,
                                            tabletWidth: 33.39999999999999857891452847979962825775146484375,
                                            paddingTop: 0,
                                            paddingTopSuffix: "px",
                                            padding: 15,
                                            paddingBottom: 0,
                                            paddingBottomSuffix: "px"
                                        }
                                    },
                                    {
                                        type: "Column",
                                        value: {
                                            _styles: [
                                                "column"
                                            ],
                                            items: [
                                                {
                                                    type: "Cloneable",
                                                    value: {
                                                        _styles: [
                                                            "wrapper-clone",
                                                            "wrapper-clone--icon"
                                                        ],
                                                        items: [
                                                            {
                                                                type: "Icon",
                                                                value: {
                                                                    _styles: [
                                                                        "icon"
                                                                    ],
                                                                    name: "logo-fb-simple",
                                                                    type: "glyph",
                                                                    size: "custom",
                                                                    customSize: 18,
                                                                    borderRadius: 0,
                                                                    colorPalette: "",
                                                                    colorHex: "#2e4da7",
                                                                    colorOpacity: 1,
                                                                    hoverColorPalette: "",
                                                                    hoverColorHex: "#496cd1",
                                                                    hoverColorOpacity: 0.8000000000000000444089209850062616169452667236328125,
                                                                    tabletSize: "custom",
                                                                    tabletCustomSize: 18,
                                                                    mobileCustomSize: 24
                                                                }
                                                            },
                                                            {
                                                                type: "Icon",
                                                                value: {
                                                                    _styles: [
                                                                        "icon"
                                                                    ],
                                                                    name: "logo-pinterest",
                                                                    type: "glyph",
                                                                    size: "custom",
                                                                    customSize: 18,
                                                                    borderRadius: 0,
                                                                    colorPalette: "",
                                                                    colorHex: "#bd081c",
                                                                    colorOpacity: 1,
                                                                    hoverColorPalette: "",
                                                                    hoverColorHex: "#d63d4e",
                                                                    hoverColorOpacity: 0.8000000000000000444089209850062616169452667236328125,
                                                                    tabletSize: "custom",
                                                                    tabletCustomSize: 18,
                                                                    mobileCustomSize: 24
                                                                }
                                                            },
                                                            {
                                                                type: "Icon",
                                                                value: {
                                                                    _styles: [
                                                                        "icon"
                                                                    ],
                                                                    name: "logo-twitter",
                                                                    type: "glyph",
                                                                    size: "custom",
                                                                    customSize: 18,
                                                                    borderRadius: 0,
                                                                    colorPalette: "",
                                                                    colorHex: "#1d9ceb",
                                                                    colorOpacity: 1,
                                                                    hoverColorPalette: "",
                                                                    hoverColorHex: "#47aae8",
                                                                    hoverColorOpacity: 0.8000000000000000444089209850062616169452667236328125,
                                                                    tabletSize: "custom",
                                                                    tabletCustomSize: 18,
                                                                    mobileCustomSize: 24
                                                                }
                                                            }
                                                        ],
                                                        horizontalAlign: "right",
                                                        itemPadding: 16,
                                                        itemPaddingRight: 16,
                                                        itemPaddingLeft: 16,
                                                        mobileHorizontalAlign: "center",
                                                        marginType: "grouped",
                                                        tabletMarginType: "grouped"
                                                    }
                                                }
                                            ],
                                            width: 33.39999999999999857891452847979962825775146484375,
                                            verticalAlign: "center",
                                            tabletWidth: 33.10000000000000142108547152020037174224853515625,
                                            paddingRight: 0,
                                            paddingRightSuffix: "px",
                                            padding: 0,
                                            paddingLeft: 0,
                                            paddingLeftSuffix: "px",
                                            paddingTop: 0,
                                            paddingTopSuffix: "px",
                                            paddingBottom: 0,
                                            paddingBottomSuffix: "px",
                                            mobileWidth: 100
                                        }
                                    }
                                ],
                                padding: 0,
                                paddingSuffix: "px",
                                paddingTop: 0,
                                paddingRight: 0,
                                paddingBottom: 0,
                                paddingLeft: 0,
                                size: 100,
                                tabletPaddingRight: 0,
                                tabletPaddingRightSuffix: "px",
                                tabletPadding: 0,
                                tabletPaddingLeft: 0,
                                tabletPaddingLeftSuffix: "px"
                            }
                        }
                    ],
                    paddingType: "ungrouped",
                    paddingTop: 15,
                    paddingBottom: 15,
                    padding: 75,
                    bgColorHex: "",
                    bgColorOpacity: 1,
                    bgColorPalette: "color1",
                    tempBgColorOpacity: 1,
                    tabletPaddingType: "ungrouped",
                    tabletPaddingTop: 15,
                    tabletPaddingBottom: 15,
                    tabletPadding: 50,
                    mobilePaddingType: "ungrouped",
                    mobilePaddingTop: 15,
                    mobilePaddingBottom: 15,
                    mobilePadding: 25,
                    boxShadow: "on",
                    boxShadowVertical: 0,
                    boxShadowColorOpacity: 0.1499999999999999944488848768742172978818416595458984375,
                    boxShadowColorHex: "#000000",
                    boxShadowColorPalette: "",
                    boxShadowBlur: 15
                }
            },
            {
                type: "SectionHeaderStickyItem",
                value: {
                    _styles: [
                        "sectionHeaderStickyItem"
                    ],
                    items: []
                }
            }
        ],
        type: "fixed"
    }
}
};