module.exports = {
  id: "Footer001Dark",
  thumbnailWidth: 600,
  thumbnailHeight: 311,
  title: "Footer001Dark",
  keywords: "footer",
  cat: [12],
  type: 1,
  pro: true,
  resolve: {
    type: "SectionFooter",
    value: {
      _styles: ["sectionFooter"],
      items: [
        {
          type: "Row",
          value: {
            _styles: ["row"],
            items: [
              {
                type: "Column",
                value: {
                  _styles: ["column"],
                  items: []
                }
              },
              {
                type: "Column",
                value: {
                  _styles: ["column"],
                  items: []
                }
              }
            ]
          }
        }
      ]
    }
  }
};
