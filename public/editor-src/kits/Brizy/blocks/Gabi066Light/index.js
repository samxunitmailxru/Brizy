module.exports = {
id: "Gabi066Light",

thumbnailWidth: 600,
thumbnailHeight: 519,
title: "Gabi066Light",

keywords: "Post, Blog, Content, Cover",
cat: [17, 18, 20, 2],
type: 0,
pro: true,
resolve: {
    type: "Section",
    value: {
        _styles: [
            "section"
        ],
        items: [
            {
                type: "SectionItem",
                value: {
                    _styles: [
                        "section-item"
                    ],
                    items: [
                        {
                            type: "Row",
                            value: {
                                _styles: [
                                    "row"
                                ],
                                items: [
                                    {
                                        type: "Column",
                                        value: {
                                            _styles: [
                                                "column"
                                            ],
                                            items: [
                                                {
                                                    type: "Wrapper",
                                                    value: {
                                                        _styles: [
                                                            "wrapper",
                                                            "wrapper--richText"
                                                        ],
                                                        items: [
                                                            {
                                                                type: "RichText",
                                                                value: {
                                                                    _styles: [
                                                                        "richText"
                                                                    ],
                                                                    text: "<p class=\"brz-mb-lg-20 brz-tp-button\"><span class=\"brz-cp-color5\">DESTINATIONS / TRAVEL</span></p>"
                                                                }
                                                            }
                                                        ]
                                                    }
                                                },
                                                {
                                                    type: "Wrapper",
                                                    value: {
                                                        _styles: [
                                                            "wrapper",
                                                            "wrapper--richText"
                                                        ],
                                                        items: [
                                                            {
                                                                type: "RichText",
                                                                value: {
                                                                    _styles: [
                                                                        "richText"
                                                                    ],
                                                                    text: "<p class=\"brz-fs-im-38 brz-fs-lg-60 brz-fw-im-800 brz-fw-lg-800 brz-ff-montserrat brz-ls-im-m_1 brz-ls-lg-m_1_5 brz-lh-im-1_3 brz-lh-lg-1_3 brz-mb-lg-10\"><span style=\"color: rgb(255, 255, 255);\">Why I continued my round-the-world cycling trip even after losing myself</span></p>"
                                                                }
                                                            }
                                                        ]
                                                    }
                                                },
                                                {
                                                    type: "Wrapper",
                                                    value: {
                                                        _styles: [
                                                            "wrapper",
                                                            "wrapper--richText"
                                                        ],
                                                        items: [
                                                            {
                                                                type: "RichText",
                                                                value: {
                                                                    _styles: [
                                                                        "richText"
                                                                    ],
                                                                    text: "<p class=\"brz-text-lg-left brz-tp-paragraph\"><span class=\"brz-cp-color6\" style=\"opacity: 0.7;\">The experience did, however, give me a different perspective that challenged the expected \u201cnorm\u201d that I\u2019d find if I just read news headlines.</span></p>"
                                                                }
                                                            }
                                                        ],
                                                        paddingType: "ungrouped",
                                                        paddingRight: 260,
                                                        paddingRightSuffix: "px",
                                                        padding: 0
                                                    }
                                                },
                                                {
                                                    type: "Wrapper",
                                                    value: {
                                                        _styles: [
                                                            "wrapper",
                                                            "wrapper--spacer"
                                                        ],
                                                        items: [
                                                            {
                                                                type: "Spacer",
                                                                value: {
                                                                    _styles: [
                                                                        "spacer"
                                                                    ],
                                                                    height: 32
                                                                }
                                                            }
                                                        ],
                                                        showOnMobile: "off"
                                                    }
                                                },
                                                {
                                                    type: "Row",
                                                    value: {
                                                        _styles: [
                                                            "row",
                                                            "hide-row-borders",
                                                            "padding-0"
                                                        ],
                                                        items: [
                                                            {
                                                                type: "Column",
                                                                value: {
                                                                    _styles: [
                                                                        "column"
                                                                    ],
                                                                    items: [
                                                                        {
                                                                            type: "Wrapper",
                                                                            value: {
                                                                                _styles: [
                                                                                    "wrapper",
                                                                                    "wrapper--iconText"
                                                                                ],
                                                                                items: [
                                                                                    {
                                                                                        type: "IconText",
                                                                                        value: {
                                                                                            _styles: [
                                                                                                "iconText"
                                                                                            ],
                                                                                            items: [
                                                                                                {
                                                                                                    type: "Icon",
                                                                                                    value: {
                                                                                                        _styles: [
                                                                                                            "icon",
                                                                                                            "iconText--icon"
                                                                                                        ],
                                                                                                        name: "pin-3",
                                                                                                        type: "glyph",
                                                                                                        size: "custom",
                                                                                                        customSize: 16,
                                                                                                        borderRadius: 0
                                                                                                    }
                                                                                                },
                                                                                                {
                                                                                                    type: "RichText",
                                                                                                    value: {
                                                                                                        _styles: [
                                                                                                            "text",
                                                                                                            "iconText--text"
                                                                                                        ],
                                                                                                        text: "<p class=\"brz-tp-button\"><span class=\"brz-cp-color8\">SEP 8, 2018</span></p>"
                                                                                                    }
                                                                                                }
                                                                                            ],
                                                                                            iconSpacing: 7
                                                                                        }
                                                                                    }
                                                                                ]
                                                                            }
                                                                        }
                                                                    ],
                                                                    paddingLeft: 0,
                                                                    paddingLeftSuffix: "px",
                                                                    padding: 15,
                                                                    width: 17.5
                                                                }
                                                            },
                                                            {
                                                                type: "Column",
                                                                value: {
                                                                    _styles: [
                                                                        "column"
                                                                    ],
                                                                    items: [
                                                                        {
                                                                            type: "Wrapper",
                                                                            value: {
                                                                                _styles: [
                                                                                    "wrapper",
                                                                                    "wrapper--iconText"
                                                                                ],
                                                                                items: [
                                                                                    {
                                                                                        type: "IconText",
                                                                                        value: {
                                                                                            _styles: [
                                                                                                "iconText"
                                                                                            ],
                                                                                            items: [
                                                                                                {
                                                                                                    type: "Icon",
                                                                                                    value: {
                                                                                                        _styles: [
                                                                                                            "icon",
                                                                                                            "iconText--icon"
                                                                                                        ],
                                                                                                        name: "heart-2",
                                                                                                        type: "glyph",
                                                                                                        size: "custom",
                                                                                                        customSize: 15,
                                                                                                        borderRadius: 0
                                                                                                    }
                                                                                                },
                                                                                                {
                                                                                                    type: "RichText",
                                                                                                    value: {
                                                                                                        _styles: [
                                                                                                            "text",
                                                                                                            "iconText--text"
                                                                                                        ],
                                                                                                        text: "<p class=\"brz-tp-button\"><span class=\"brz-cp-color8\">469</span></p>"
                                                                                                    }
                                                                                                }
                                                                                            ],
                                                                                            iconSpacing: 7
                                                                                        }
                                                                                    }
                                                                                ]
                                                                            }
                                                                        }
                                                                    ],
                                                                    width: 11.699999999999999289457264239899814128875732421875
                                                                }
                                                            },
                                                            {
                                                                type: "Column",
                                                                value: {
                                                                    _styles: [
                                                                        "column"
                                                                    ],
                                                                    items: [
                                                                        {
                                                                            type: "Cloneable",
                                                                            value: {
                                                                                _styles: [
                                                                                    "wrapper-clone",
                                                                                    "wrapper-clone--button"
                                                                                ],
                                                                                items: [
                                                                                    {
                                                                                        type: "Button",
                                                                                        value: {
                                                                                            _styles: [
                                                                                                "button"
                                                                                            ],
                                                                                            fillType: "outline",
                                                                                            tempFillType: "outline",
                                                                                            paddingRL: 26,
                                                                                            paddingRight: 26,
                                                                                            paddingLeft: 26,
                                                                                            paddingTB: 8,
                                                                                            paddingTop: 8,
                                                                                            paddingBottom: 8,
                                                                                            borderRadiusType: "rounded",
                                                                                            borderRadius: 21,
                                                                                            borderWidth: 1,
                                                                                            borderColorOpacity: 1,
                                                                                            borderColorPalette: "",
                                                                                            bgColorOpacity: 0,
                                                                                            bgColorPalette: "color3",
                                                                                            hoverBgColorOpacity: 0.8000000000000000444089209850062616169452667236328125,
                                                                                            hoverBorderColorOpacity: 0.8000000000000000444089209850062616169452667236328125,
                                                                                            mobilePaddingRL: 26,
                                                                                            mobilePaddingRight: 26,
                                                                                            mobilePaddingLeft: 26,
                                                                                            borderColorHex: "#ffffff",
                                                                                            tempBorderColorOpacity: 1,
                                                                                            tempBorderColorPalette: "",
                                                                                            hoverBgColorHex: "#224d62",
                                                                                            tempBorderRadiusType: "rounded",
                                                                                            mobileBorderRadius: 23,
                                                                                            tempBorderWidth: 2,
                                                                                            size: "custom",
                                                                                            mobileSize: "small",
                                                                                            fontSize: 12,
                                                                                            tempPaddingTB: 8,
                                                                                            tempPaddingTop: 8,
                                                                                            tempPaddingBottom: 8,
                                                                                            tempPaddingRL: 26,
                                                                                            tempPaddingRight: 26,
                                                                                            tempPaddingLeft: 26,
                                                                                            mobilePaddingTop: 11,
                                                                                            mobilePaddingBottom: 11,
                                                                                            iconPosition: "left",
                                                                                            iconName: "curved-next",
                                                                                            iconType: "glyph",
                                                                                            iconSize: "custom",
                                                                                            iconCustomSize: 13,
                                                                                            text: "34 SHARES"
                                                                                        }
                                                                                    }
                                                                                ],
                                                                                horizontalAlign: "left",
                                                                                marginTop: 5,
                                                                                marginTopSuffix: "px",
                                                                                margin: 0
                                                                            }
                                                                        }
                                                                    ],
                                                                    width: 70.7999999999999971578290569595992565155029296875,
                                                                    verticalAlign: "center",
                                                                    paddingTop: 0,
                                                                    paddingTopSuffix: "px",
                                                                    padding: 15,
                                                                    paddingBottom: 0,
                                                                    paddingBottomSuffix: "px"
                                                                }
                                                            }
                                                        ]
                                                    }
                                                }
                                            ],
                                            width: 100,
                                            paddingRight: 100,
                                            paddingRightSuffix: "px",
                                            padding: 15
                                        }
                                    }
                                ],
                                size: 90
                            }
                        }
                    ],
                    bgImageWidth: 1920,
                    bgImageHeight: 1080,
                    bgImageSrc: "a1c3596ccb312a420b0a7cac1ac11bc9.jpg",
                    bgPositionX: 54,
                    bgPositionY: 83,
                    bgColorOpacity: 0,
                    tempBgColorOpacity: 0.1000000000000000055511151231257827021181583404541015625,
                    tempMobileBgColorOpacity: 1,
                    paddingType: "ungrouped",
                    paddingTop: 100,
                    paddingBottom: 100,
                    padding: 75,
                    containerType: "boxed",
                    bgColorPalette: "color1",
                    bgColorHex: "#191b21"
                }
            }
        ]
    }
}
};