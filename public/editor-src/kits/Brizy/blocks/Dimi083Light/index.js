module.exports = {
id: "Dimi083Light",

thumbnailWidth: 750,
thumbnailHeight: 124,
title: "Dimi083Light",

keywords: "Header",
cat: [21],
type: 0,
pro: true,
resolve: {
    type: "SectionHeader",
    value: {
        _styles: [
            "sectionHeader"
        ],
        items: [
            {
                type: "SectionHeaderItem",
                value: {
                    _styles: [
                        "sectionHeaderItem"
                    ],
                    items: [
                        {
                            type: "Row",
                            value: {
                                _styles: [
                                    "row"
                                ],
                                items: [
                                    {
                                        type: "Column",
                                        value: {
                                            _styles: [
                                                "column"
                                            ],
                                            items: [
                                                {
                                                    type: "Cloneable",
                                                    value: {
                                                        _styles: [
                                                            "wrapper-clone",
                                                            "wrapper-clone--icon"
                                                        ],
                                                        items: [
                                                            {
                                                                type: "Icon",
                                                                value: {
                                                                    _styles: [
                                                                        "icon"
                                                                    ],
                                                                    name: "logo-fb-simple",
                                                                    type: "glyph",
                                                                    size: "custom",
                                                                    customSize: 18,
                                                                    borderRadius: 0,
                                                                    colorPalette: "",
                                                                    colorHex: "#2e4da7",
                                                                    colorOpacity: 1,
                                                                    hoverColorPalette: "",
                                                                    hoverColorHex: "#496cd1",
                                                                    hoverColorOpacity: 0.8000000000000000444089209850062616169452667236328125,
                                                                    tabletSize: "custom",
                                                                    tabletCustomSize: 18,
                                                                    mobileCustomSize: 24
                                                                }
                                                            },
                                                            {
                                                                type: "Icon",
                                                                value: {
                                                                    _styles: [
                                                                        "icon"
                                                                    ],
                                                                    name: "logo-pinterest",
                                                                    type: "glyph",
                                                                    size: "custom",
                                                                    customSize: 18,
                                                                    borderRadius: 0,
                                                                    colorPalette: "",
                                                                    colorHex: "#bd081c",
                                                                    colorOpacity: 1,
                                                                    hoverColorPalette: "",
                                                                    hoverColorHex: "#d63d4e",
                                                                    hoverColorOpacity: 0.8000000000000000444089209850062616169452667236328125,
                                                                    tabletSize: "custom",
                                                                    tabletCustomSize: 18,
                                                                    mobileCustomSize: 24
                                                                }
                                                            },
                                                            {
                                                                type: "Icon",
                                                                value: {
                                                                    _styles: [
                                                                        "icon"
                                                                    ],
                                                                    name: "logo-twitter",
                                                                    type: "glyph",
                                                                    size: "custom",
                                                                    customSize: 18,
                                                                    borderRadius: 0,
                                                                    colorPalette: "",
                                                                    colorHex: "#1d9ceb",
                                                                    colorOpacity: 1,
                                                                    hoverColorPalette: "",
                                                                    hoverColorHex: "#47aae8",
                                                                    hoverColorOpacity: 0.8000000000000000444089209850062616169452667236328125,
                                                                    tabletSize: "custom",
                                                                    tabletCustomSize: 18,
                                                                    mobileCustomSize: 24
                                                                }
                                                            }
                                                        ],
                                                        horizontalAlign: "left",
                                                        itemPadding: 16,
                                                        itemPaddingRight: 16,
                                                        itemPaddingLeft: 16,
                                                        mobileHorizontalAlign: "center",
                                                        marginType: "grouped",
                                                        tabletMarginType: "grouped"
                                                    }
                                                }
                                            ],
                                            width: 50,
                                            verticalAlign: "center",
                                            tabletWidth: 26.5,
                                            paddingRight: 0,
                                            paddingRightSuffix: "px",
                                            padding: 0,
                                            paddingLeft: 0,
                                            paddingLeftSuffix: "px",
                                            paddingTop: 0,
                                            paddingTopSuffix: "px",
                                            paddingBottom: 0,
                                            paddingBottomSuffix: "px",
                                            mobileWidth: 33.89999999999999857891452847979962825775146484375
                                        }
                                    },
                                    {
                                        type: "Column",
                                        value: {
                                            _styles: [
                                                "column"
                                            ],
                                            items: [
                                                {
                                                    type: "Cloneable",
                                                    value: {
                                                        _styles: [
                                                            "wrapper-clone",
                                                            "wrapper-clone--button"
                                                        ],
                                                        items: [
                                                            {
                                                                type: "Button",
                                                                value: {
                                                                    _styles: [
                                                                        "button"
                                                                    ],
                                                                    text: "CALL US: +1 585 69 45",
                                                                    fillType: "default",
                                                                    tempFillType: "filled",
                                                                    paddingRL: 0,
                                                                    paddingRight: 0,
                                                                    paddingLeft: 0,
                                                                    paddingTB: 14,
                                                                    paddingTop: 14,
                                                                    paddingBottom: 14,
                                                                    borderRadiusType: "",
                                                                    borderRadius: 2,
                                                                    borderWidth: 0,
                                                                    borderColorOpacity: 0,
                                                                    borderColorPalette: "",
                                                                    bgColorOpacity: 0,
                                                                    bgColorPalette: "color1",
                                                                    hoverBgColorOpacity: 0,
                                                                    hoverBorderColorOpacity: 0,
                                                                    tempBgColorPalette: "color1",
                                                                    tempBorderColorPalette: "",
                                                                    bgColorHex: "#191b21",
                                                                    tempBgColorOpacity: 1,
                                                                    borderColorHex: "#239ddb",
                                                                    hoverBorderColorHex: "#191b21",
                                                                    colorPalette: "color3",
                                                                    colorOpacity: 1,
                                                                    iconName: "phone-2",
                                                                    iconType: "glyph",
                                                                    hoverColorPalette: "color1",
                                                                    hoverColorOpacity: 1,
                                                                    iconPosition: "left",
                                                                    iconSize: "custom",
                                                                    iconCustomSize: 13,
                                                                    fontSize: 15,
                                                                    fontFamily: "roboto",
                                                                    lineHeight: 1.5,
                                                                    letterSpacing: 0,
                                                                    fontWeight: 700,
                                                                    fontStyle: "",
                                                                    colorHex: "#db5d23",
                                                                    tempColorOpacity: 1,
                                                                    hoverColorHex: "#db5d23",
                                                                    tabletFontSize: 15,
                                                                    tabletLineHeight: 1.5,
                                                                    tabletLetterSpacing: 0,
                                                                    tabletFontWeight: 700,
                                                                    tabletFontStyle: ""
                                                                }
                                                            },
                                                            {
                                                                type: "Button",
                                                                value: {
                                                                    _styles: [
                                                                        "button"
                                                                    ],
                                                                    text: "GET A QUOTE",
                                                                    fillType: "default",
                                                                    tempFillType: "filled",
                                                                    paddingRL: 0,
                                                                    paddingRight: 0,
                                                                    paddingLeft: 0,
                                                                    paddingTB: 14,
                                                                    paddingTop: 14,
                                                                    paddingBottom: 14,
                                                                    borderRadiusType: "",
                                                                    borderRadius: 2,
                                                                    borderWidth: 0,
                                                                    borderColorOpacity: 0,
                                                                    borderColorPalette: "",
                                                                    bgColorOpacity: 0,
                                                                    bgColorPalette: "color1",
                                                                    hoverBgColorOpacity: 0,
                                                                    hoverBorderColorOpacity: 0,
                                                                    tempBgColorPalette: "color1",
                                                                    tempBorderColorPalette: "",
                                                                    bgColorHex: "#191b21",
                                                                    tempBgColorOpacity: 1,
                                                                    borderColorHex: "#239ddb",
                                                                    hoverBorderColorHex: "#191b21",
                                                                    colorPalette: "color3",
                                                                    colorOpacity: 1,
                                                                    iconName: "coins",
                                                                    iconType: "glyph",
                                                                    hoverColorPalette: "color1",
                                                                    hoverColorOpacity: 1,
                                                                    iconPosition: "left",
                                                                    iconSize: "custom",
                                                                    iconCustomSize: 13,
                                                                    fontSize: 15,
                                                                    fontFamily: "roboto",
                                                                    lineHeight: 1.5,
                                                                    letterSpacing: 0,
                                                                    fontWeight: 700,
                                                                    fontStyle: "",
                                                                    colorHex: "#4b4b4b",
                                                                    tabletFontSize: 15,
                                                                    tabletLineHeight: 1.5,
                                                                    tabletLetterSpacing: 0,
                                                                    tabletFontWeight: 700,
                                                                    tabletFontStyle: ""
                                                                }
                                                            }
                                                        ],
                                                        itemPadding: 38,
                                                        itemPaddingRight: 38,
                                                        itemPaddingLeft: 38,
                                                        horizontalAlign: "right",
                                                        marginType: "grouped",
                                                        tabletHorizontalAlign: "right",
                                                        tabletMarginType: "grouped",
                                                        showOnTablet: "on"
                                                    }
                                                }
                                            ],
                                            verticalAlign: "center",
                                            width: 50,
                                            paddingTop: 0,
                                            paddingTopSuffix: "px",
                                            padding: 0,
                                            paddingBottom: 0,
                                            paddingBottomSuffix: "px",
                                            tabletWidth: 73.099999999999994315658113919198513031005859375,
                                            paddingType: "grouped",
                                            paddingSuffix: "px",
                                            paddingRight: 0,
                                            paddingLeft: 0,
                                            tabletPaddingType: "grouped",
                                            tabletPadding: 0,
                                            tabletPaddingSuffix: "px",
                                            tabletPaddingTop: 0,
                                            tabletPaddingRight: 0,
                                            tabletPaddingBottom: 0,
                                            tabletPaddingLeft: 0,
                                            mobileWidth: 65.900000000000005684341886080801486968994140625
                                        }
                                    }
                                ],
                                borderWidthType: "ungrouped",
                                borderBottomWidth: 1,
                                borderWidth: 4,
                                tempBorderRightWidth: 0,
                                tempBorderTopWidth: 0,
                                tempBorderLeftWidth: 0,
                                tempBorderBottomWidth: 1,
                                tempBorderWidth: 4,
                                borderColorOpacity: 0.14000000000000001332267629550187848508358001708984375,
                                borderColorHex: "#7c8087",
                                borderColorPalette: "",
                                tempBorderColorOpacity: 0.14000000000000001332267629550187848508358001708984375,
                                borderTopWidth: 0,
                                borderRightWidth: 0,
                                borderLeftWidth: 0,
                                borderRadius: 0,
                                borderTopLeftRadius: 0,
                                borderTopRightRadius: 0,
                                borderBottomRightRadius: 0,
                                borderBottomLeftRadius: 0,
                                tabletPaddingRight: 10,
                                tabletPaddingLeft: 10,
                                mobilePaddingRight: 10,
                                mobilePaddingLeft: 10,
                                padding: 0,
                                paddingSuffix: "px",
                                paddingTop: 0,
                                paddingRight: 5,
                                paddingBottom: 0,
                                paddingLeft: 5,
                                paddingType: "ungrouped",
                                paddingBottomSuffix: "px",
                                paddingTopSuffix: "px",
                                paddingRightSuffix: "px",
                                paddingLeftSuffix: "px",
                                showOnTablet: "on",
                                tabletPaddingBottom: 10,
                                tabletPaddingBottomSuffix: "px",
                                tabletPadding: 10,
                                tabletPaddingTop: 10,
                                tabletPaddingTopSuffix: "px",
                                showOnMobile: "off"
                            }
                        },
                        {
                            type: "Row",
                            value: {
                                _styles: [
                                    "row"
                                ],
                                items: [
                                    {
                                        type: "Column",
                                        value: {
                                            _styles: [
                                                "column"
                                            ],
                                            items: [
                                                {
                                                    type: "Wrapper",
                                                    value: {
                                                        _styles: [
                                                            "wrapper",
                                                            "wrapper--image"
                                                        ],
                                                        items: [
                                                            {
                                                                type: "Image",
                                                                value: {
                                                                    _styles: [
                                                                        "image"
                                                                    ],
                                                                    imageWidth: 2129,
                                                                    imageHeight: 457,
                                                                    imageSrc: "b731902c6bf15805972db395c1ec4670.png",
                                                                    height: 100,
                                                                    positionX: 50,
                                                                    positionY: 50,
                                                                    imagePopulation: "",
                                                                    resize: 73,
                                                                    mobileResize: 67,
                                                                    tabletPositionX: 52,
                                                                    tabletPositionY: 56,
                                                                    tabletHeight: 100,
                                                                    tabletResize: 95
                                                                }
                                                            }
                                                        ],
                                                        horizontalAlign: "left",
                                                        mobileHorizontalAlign: "left",
                                                        marginType: "grouped"
                                                    }
                                                }
                                            ],
                                            width: 18,
                                            verticalAlign: "center",
                                            paddingTop: 0,
                                            paddingTopSuffix: "px",
                                            padding: 0,
                                            paddingBottom: 0,
                                            paddingBottomSuffix: "px",
                                            paddingType: "grouped",
                                            paddingSuffix: "px",
                                            paddingRight: 0,
                                            paddingLeft: 0,
                                            tabletWidth: 26.39999999999999857891452847979962825775146484375,
                                            mobileWidth: 58.2000000000000028421709430404007434844970703125
                                        }
                                    },
                                    {
                                        type: "Column",
                                        value: {
                                            _styles: [
                                                "column"
                                            ],
                                            items: [
                                                {
                                                    type: "Wrapper",
                                                    value: {
                                                        _styles: [
                                                            "wrapper",
                                                            "wrapper--menu"
                                                        ],
                                                        items: [
                                                            {
                                                                type: "Menu",
                                                                value: {
                                                                    _styles: [
                                                                        "menu"
                                                                    ],
                                                                    menuSelected: "fd0a3787640c4c823dcfb02fe68e414d",
                                                                    fontSize: 12,
                                                                    fontFamily: "roboto",
                                                                    lineHeight: 1.5,
                                                                    letterSpacing: 2,
                                                                    fontWeight: 700,
                                                                    fontStyle: "",
                                                                    itemPadding: 49,
                                                                    itemPaddingRight: 49,
                                                                    itemPaddingLeft: 49,
                                                                    colorPalette: "",
                                                                    colorOpacity: 1,
                                                                    hoverColorPalette: "color3",
                                                                    colorHex: "#4b4b4b",
                                                                    tempColorOpacity: 1,
                                                                    hoverColorHex: "#4b4b4b",
                                                                    hoverColorOpacity: 1,
                                                                    mobileMMenu: "on",
                                                                    mobileFontSize: 18,
                                                                    mobileLineHeight: 2,
                                                                    mobileLetterSpacing: 0,
                                                                    mobileFontWeight: 600,
                                                                    mobileFontStyle: "",
                                                                    tabletMMenu: "on",
                                                                    tabletFontSize: 12,
                                                                    tabletLineHeight: 1.5,
                                                                    tabletLetterSpacing: 2,
                                                                    tabletFontWeight: 700,
                                                                    tabletFontStyle: "",
                                                                    mobileMMenuSize: 23,
                                                                    mMenuTitle: "Main Menu",
                                                                    mMenu: "off",
                                                                    mMenuSize: 23,
                                                                    mMenuFontSize: 13,
                                                                    mMenuFontFamily: "roboto",
                                                                    mMenuLineHeight: 3,
                                                                    mMenuLetterSpacing: 1.5,
                                                                    mMenuFontWeight: 500,
                                                                    mMenuFontStyle: "",
                                                                    mMenuBorderColorHex: "#ffffff",
                                                                    mMenuBorderColorOpacity: 0.08000000000000000166533453693773481063544750213623046875,
                                                                    mMenuBorderColorPalette: "",
                                                                    mMenuTempBorderColorOpacity: 0.08000000000000000166533453693773481063544750213623046875,
                                                                    mMenuBgColorPalette: "color1",
                                                                    mMenuBgColorOpacity: 1,
                                                                    mMenuBgColorHex: "#ffffff",
                                                                    mMenuTempBgColorOpacity: 1,
                                                                    mMenuHoverColorPalette: "color3",
                                                                    mMenuHoverColorOpacity: 1,
                                                                    mMenuItemHorizontalAlign: "left",
                                                                    tabletMMenuFontSize: 13,
                                                                    tabletMMenuLineHeight: 3,
                                                                    tabletMMenuLetterSpacing: 1.5,
                                                                    tabletMMenuFontWeight: 500,
                                                                    tabletMMenuFontStyle: "",
                                                                    mobileMMenuFontSize: 13,
                                                                    mobileMMenuLineHeight: 3,
                                                                    mobileMMenuLetterSpacing: 1.5,
                                                                    mobileMMenuFontWeight: 500,
                                                                    mobileMMenuFontStyle: "",
                                                                    tabletHorizontalAlign: "right",
                                                                    mMenuPosition: "left",
                                                                    tabletMMenuSize: 23,
                                                                    subMenuBgColorHex: "#ffffff",
                                                                    subMenuBgColorOpacity: 1,
                                                                    subMenuBgColorPalette: "",
                                                                    subMenuTempBgColorOpacity: 1,
                                                                    subMenuHoverBgColorHex: "#ffffff",
                                                                    subMenuHoverBgColorOpacity: "#927474",
                                                                    subMenuBorderColorHex: "#000000",
                                                                    subMenuBorderColorOpacity: 0.08000000000000000166533453693773481063544750213623046875,
                                                                    subMenuBorderColorPalette: "",
                                                                    subMenuTempBorderColorOpacity: 0.08000000000000000166533453693773481063544750213623046875,
                                                                    subMenuHoverBorderColorHex: "#000000",
                                                                    subMenuHoverBorderColorOpacity: 0.08000000000000000166533453693773481063544750213623046875,
                                                                    subMenuColorPalette: "color1",
                                                                    subMenuColorOpacity: 1,
                                                                    subMenuHoverColorPalette: "color3",
                                                                    mMenuColorPalette: "",
                                                                    subMenuFontSize: 15,
                                                                    subMenuFontFamily: "roboto",
                                                                    subMenuLineHeight: 1.3000000000000000444089209850062616169452667236328125,
                                                                    subMenuLetterSpacing: 0,
                                                                    subMenuFontWeight: 400,
                                                                    subMenuFontStyle: "",
                                                                    subMenuHoverBorderColorPalette: "",
                                                                    subMenuTempHoverBorderColorOpacity: 0.08000000000000000166533453693773481063544750213623046875,
                                                                    subMenuHoverColorOpacity: 1,
                                                                    mMenuColorHex: "#ffffff",
                                                                    mMenuColorOpacity: 1,
                                                                    mMenuTempColorOpacity: 1,
                                                                    mMenuHoverColorHex: "#ffffff",
                                                                    horizontalAlign: "right",
                                                                    mobileMMenuItemHorizontalAlign: "left"
                                                                }
                                                            }
                                                        ],
                                                        showOnMobile: "on",
                                                        showOnTablet: "on",
                                                        tabletHorizontalAlign: "right",
                                                        mobileMarginTop: 0,
                                                        mobileMarginTopSuffix: "px",
                                                        mobileMargin: 0,
                                                        marginType: "grouped"
                                                    }
                                                }
                                            ],
                                            verticalAlign: "center",
                                            width: 82,
                                            paddingTop: 0,
                                            paddingTopSuffix: "px",
                                            padding: 0,
                                            paddingBottom: 0,
                                            paddingBottomSuffix: "px",
                                            tabletWidth: 73.099999999999994315658113919198513031005859375,
                                            paddingType: "grouped",
                                            paddingSuffix: "px",
                                            paddingRight: 0,
                                            paddingLeft: 0,
                                            mobileWidth: 41.10000000000000142108547152020037174224853515625
                                        }
                                    }
                                ],
                                padding: 0,
                                paddingSuffix: "px",
                                paddingTop: 20,
                                paddingRight: 5,
                                paddingBottom: 20,
                                paddingLeft: 5,
                                paddingType: "ungrouped",
                                paddingTopSuffix: "px",
                                paddingBottomSuffix: "px",
                                paddingRightSuffix: "px",
                                paddingLeftSuffix: "px"
                            }
                        }
                    ],
                    paddingType: "grouped",
                    paddingTop: 0,
                    paddingBottom: 0,
                    padding: 0,
                    bgColorHex: "",
                    bgColorOpacity: 1,
                    bgColorPalette: "color8",
                    tempBgColorOpacity: 1,
                    tabletPaddingType: "grouped",
                    tabletPaddingTop: 0,
                    tabletPaddingBottom: 0,
                    tabletPadding: 0,
                    mobilePaddingType: "ungrouped",
                    mobilePaddingTop: 15,
                    mobilePaddingBottom: 15,
                    mobilePadding: 25,
                    containerType: "boxed",
                    mobileBgColorHex: "#ffffff",
                    mobileBgColorOpacity: 1,
                    mobileBgColorPalette: "",
                    boxShadow: "on",
                    boxShadowVertical: 0,
                    boxShadowColorOpacity: 0.1000000000000000055511151231257827021181583404541015625,
                    boxShadowBlur: 10,
                    boxShadowColorHex: "#000000",
                    boxShadowColorPalette: ""
                }
            },
            {
                type: "SectionHeaderStickyItem",
                value: {
                    _styles: [
                        "sectionHeaderStickyItem"
                    ],
                    items: [
                        {
                            type: "Row",
                            value: {
                                _styles: [
                                    "row"
                                ],
                                items: [
                                    {
                                        type: "Column",
                                        value: {
                                            _styles: [
                                                "column"
                                            ],
                                            items: [
                                                {
                                                    type: "Wrapper",
                                                    value: {
                                                        _styles: [
                                                            "wrapper",
                                                            "wrapper--image"
                                                        ],
                                                        items: [
                                                            {
                                                                type: "Image",
                                                                value: {
                                                                    _styles: [
                                                                        "image"
                                                                    ],
                                                                    imageWidth: 643,
                                                                    imageHeight: 457,
                                                                    imageSrc: "0232fc1ad248569faa5f648cfaf8f250.png",
                                                                    height: 100,
                                                                    positionX: 50,
                                                                    positionY: 50,
                                                                    imagePopulation: "",
                                                                    resize: 70,
                                                                    mobileResize: 15,
                                                                    tabletPositionX: 52,
                                                                    tabletPositionY: 56,
                                                                    tabletHeight: 100,
                                                                    tabletResize: 38
                                                                }
                                                            }
                                                        ],
                                                        horizontalAlign: "left",
                                                        mobileHorizontalAlign: "center",
                                                        marginType: "grouped"
                                                    }
                                                }
                                            ],
                                            width: 5.70000000000000017763568394002504646778106689453125,
                                            verticalAlign: "center",
                                            paddingTop: 0,
                                            paddingTopSuffix: "px",
                                            padding: 0,
                                            paddingBottom: 0,
                                            paddingBottomSuffix: "px",
                                            paddingType: "grouped",
                                            paddingSuffix: "px",
                                            paddingRight: 0,
                                            paddingLeft: 0,
                                            tabletWidth: 20.699999999999999289457264239899814128875732421875,
                                            showOnMobile: "off"
                                        }
                                    },
                                    {
                                        type: "Column",
                                        value: {
                                            _styles: [
                                                "column"
                                            ],
                                            items: [
                                                {
                                                    type: "Wrapper",
                                                    value: {
                                                        _styles: [
                                                            "wrapper",
                                                            "wrapper--menu"
                                                        ],
                                                        items: [
                                                            {
                                                                type: "Menu",
                                                                value: {
                                                                    _styles: [
                                                                        "menu"
                                                                    ],
                                                                    menuSelected: "fd0a3787640c4c823dcfb02fe68e414d",
                                                                    fontSize: 12,
                                                                    fontFamily: "roboto",
                                                                    lineHeight: 1.5,
                                                                    letterSpacing: 2,
                                                                    fontWeight: 700,
                                                                    fontStyle: "",
                                                                    itemPadding: 49,
                                                                    itemPaddingRight: 49,
                                                                    itemPaddingLeft: 49,
                                                                    colorPalette: "",
                                                                    colorOpacity: 1,
                                                                    hoverColorPalette: "color3",
                                                                    colorHex: "#4b4b4b",
                                                                    tempColorOpacity: 1,
                                                                    hoverColorHex: "#4b4b4b",
                                                                    hoverColorOpacity: 1,
                                                                    mobileMMenu: "on",
                                                                    mobileFontSize: 18,
                                                                    mobileLineHeight: 2,
                                                                    mobileLetterSpacing: 0,
                                                                    mobileFontWeight: 600,
                                                                    mobileFontStyle: "",
                                                                    tabletMMenu: "on",
                                                                    tabletFontSize: 12,
                                                                    tabletLineHeight: 1.5,
                                                                    tabletLetterSpacing: 2,
                                                                    tabletFontWeight: 700,
                                                                    tabletFontStyle: "",
                                                                    mobileMMenuSize: 23,
                                                                    mMenuTitle: "Main Menu",
                                                                    mMenu: "off",
                                                                    mMenuSize: 23,
                                                                    mMenuFontSize: 13,
                                                                    mMenuFontFamily: "roboto",
                                                                    mMenuLineHeight: 3,
                                                                    mMenuLetterSpacing: 1.5,
                                                                    mMenuFontWeight: 500,
                                                                    mMenuFontStyle: "",
                                                                    mMenuBorderColorHex: "#ffffff",
                                                                    mMenuBorderColorOpacity: 0.08000000000000000166533453693773481063544750213623046875,
                                                                    mMenuBorderColorPalette: "",
                                                                    mMenuTempBorderColorOpacity: 0.08000000000000000166533453693773481063544750213623046875,
                                                                    mMenuBgColorPalette: "color1",
                                                                    mMenuBgColorOpacity: 1,
                                                                    mMenuBgColorHex: "#191b21",
                                                                    mMenuTempBgColorOpacity: 1,
                                                                    mMenuHoverColorPalette: "color3",
                                                                    mMenuHoverColorOpacity: 1,
                                                                    mMenuItemHorizontalAlign: "center",
                                                                    tabletMMenuFontSize: 13,
                                                                    tabletMMenuLineHeight: 3,
                                                                    tabletMMenuLetterSpacing: 1.5,
                                                                    tabletMMenuFontWeight: 500,
                                                                    tabletMMenuFontStyle: "",
                                                                    mobileMMenuFontSize: 13,
                                                                    mobileMMenuLineHeight: 3,
                                                                    mobileMMenuLetterSpacing: 1.5,
                                                                    mobileMMenuFontWeight: 500,
                                                                    mobileMMenuFontStyle: "",
                                                                    tabletHorizontalAlign: "center",
                                                                    mMenuPosition: "left",
                                                                    tabletMMenuSize: 23,
                                                                    subMenuBgColorHex: "#ffffff",
                                                                    subMenuBgColorOpacity: 1,
                                                                    subMenuBgColorPalette: "",
                                                                    subMenuTempBgColorOpacity: 1,
                                                                    subMenuHoverBgColorHex: "#ffffff",
                                                                    subMenuHoverBgColorOpacity: "#927474",
                                                                    subMenuBorderColorHex: "#000000",
                                                                    subMenuBorderColorOpacity: 0.08000000000000000166533453693773481063544750213623046875,
                                                                    subMenuBorderColorPalette: "",
                                                                    subMenuTempBorderColorOpacity: 0.08000000000000000166533453693773481063544750213623046875,
                                                                    subMenuHoverBorderColorHex: "#000000",
                                                                    subMenuHoverBorderColorOpacity: 0.08000000000000000166533453693773481063544750213623046875,
                                                                    subMenuColorPalette: "color1",
                                                                    subMenuColorOpacity: 1,
                                                                    subMenuHoverColorPalette: "color3",
                                                                    mMenuColorPalette: "",
                                                                    subMenuFontSize: 15,
                                                                    subMenuFontFamily: "roboto",
                                                                    subMenuLineHeight: 1.3000000000000000444089209850062616169452667236328125,
                                                                    subMenuLetterSpacing: 0,
                                                                    subMenuFontWeight: 400,
                                                                    subMenuFontStyle: "",
                                                                    subMenuHoverBorderColorPalette: "",
                                                                    subMenuTempHoverBorderColorOpacity: 0.08000000000000000166533453693773481063544750213623046875,
                                                                    subMenuHoverColorOpacity: 1,
                                                                    mMenuColorHex: "#ffffff",
                                                                    mMenuColorOpacity: 1,
                                                                    mMenuTempColorOpacity: 1,
                                                                    mMenuHoverColorHex: "#ffffff",
                                                                    horizontalAlign: "center",
                                                                    mobileMMenuItemHorizontalAlign: "left"
                                                                }
                                                            }
                                                        ],
                                                        showOnMobile: "on",
                                                        showOnTablet: "on",
                                                        tabletHorizontalAlign: "right",
                                                        mobileMarginTop: 0,
                                                        mobileMarginTopSuffix: "px",
                                                        mobileMargin: 0,
                                                        marginType: "grouped",
                                                        mobileMarginBottom: 0,
                                                        mobileMarginBottomSuffix: "px"
                                                    }
                                                }
                                            ],
                                            verticalAlign: "center",
                                            width: 85.5,
                                            paddingTop: 0,
                                            paddingTopSuffix: "px",
                                            padding: 0,
                                            paddingBottom: 0,
                                            paddingBottomSuffix: "px",
                                            tabletWidth: 58.7000000000000028421709430404007434844970703125,
                                            paddingType: "grouped",
                                            paddingSuffix: "px",
                                            paddingRight: 0,
                                            paddingLeft: 0,
                                            mobileMarginBottom: 15,
                                            mobileMarginBottomSuffix: "px",
                                            mobileMargin: 10,
                                            mobileMarginTop: 15,
                                            mobileMarginTopSuffix: "px"
                                        }
                                    },
                                    {
                                        type: "Column",
                                        value: {
                                            _styles: [
                                                "column"
                                            ],
                                            items: [
                                                {
                                                    type: "Cloneable",
                                                    value: {
                                                        _styles: [
                                                            "wrapper-clone",
                                                            "wrapper-clone--icon"
                                                        ],
                                                        items: [
                                                            {
                                                                type: "Icon",
                                                                value: {
                                                                    _styles: [
                                                                        "icon"
                                                                    ],
                                                                    name: "logo-fb-simple",
                                                                    type: "glyph",
                                                                    size: "custom",
                                                                    customSize: 18,
                                                                    borderRadius: 0,
                                                                    colorPalette: "",
                                                                    colorHex: "#2e4da7",
                                                                    colorOpacity: 1,
                                                                    hoverColorPalette: "",
                                                                    hoverColorHex: "#496cd1",
                                                                    hoverColorOpacity: 0.8000000000000000444089209850062616169452667236328125,
                                                                    tabletSize: "custom",
                                                                    tabletCustomSize: 24,
                                                                    mobileCustomSize: 24
                                                                }
                                                            },
                                                            {
                                                                type: "Icon",
                                                                value: {
                                                                    _styles: [
                                                                        "icon"
                                                                    ],
                                                                    name: "logo-pinterest",
                                                                    type: "glyph",
                                                                    size: "custom",
                                                                    customSize: 18,
                                                                    borderRadius: 0,
                                                                    colorPalette: "",
                                                                    colorHex: "#bd081c",
                                                                    colorOpacity: 1,
                                                                    hoverColorPalette: "",
                                                                    hoverColorHex: "#d63d4e",
                                                                    hoverColorOpacity: 0.8000000000000000444089209850062616169452667236328125,
                                                                    tabletSize: "custom",
                                                                    tabletCustomSize: 24,
                                                                    mobileCustomSize: 24
                                                                }
                                                            },
                                                            {
                                                                type: "Icon",
                                                                value: {
                                                                    _styles: [
                                                                        "icon"
                                                                    ],
                                                                    name: "logo-twitter",
                                                                    type: "glyph",
                                                                    size: "custom",
                                                                    customSize: 18,
                                                                    borderRadius: 0,
                                                                    colorPalette: "",
                                                                    colorHex: "#1d9ceb",
                                                                    colorOpacity: 1,
                                                                    hoverColorPalette: "",
                                                                    hoverColorHex: "#47aae8",
                                                                    hoverColorOpacity: 0.8000000000000000444089209850062616169452667236328125,
                                                                    tabletSize: "custom",
                                                                    tabletCustomSize: 24,
                                                                    mobileCustomSize: 24
                                                                }
                                                            }
                                                        ],
                                                        horizontalAlign: "right",
                                                        itemPadding: 16,
                                                        itemPaddingRight: 16,
                                                        itemPaddingLeft: 16,
                                                        mobileHorizontalAlign: "center",
                                                        marginType: "grouped"
                                                    }
                                                }
                                            ],
                                            width: 8.800000000000000710542735760100185871124267578125,
                                            verticalAlign: "center",
                                            tabletWidth: 20.60000000000000142108547152020037174224853515625,
                                            paddingRight: 0,
                                            paddingRightSuffix: "px",
                                            padding: 0,
                                            paddingLeft: 0,
                                            paddingLeftSuffix: "px",
                                            paddingTop: 0,
                                            paddingTopSuffix: "px",
                                            paddingBottom: 0,
                                            paddingBottomSuffix: "px",
                                            mobileMarginType: "ungrouped",
                                            mobileMargin: 0,
                                            mobileMarginSuffix: "px",
                                            mobileMarginTop: 10,
                                            mobileMarginRight: 0,
                                            mobileMarginBottom: 10,
                                            mobileMarginLeft: 0,
                                            mobileMarginRightSuffix: "px",
                                            mobileMarginTopSuffix: "px",
                                            mobileMarginBottomSuffix: "px"
                                        }
                                    }
                                ],
                                padding: 0,
                                paddingSuffix: "px",
                                paddingTop: 20,
                                paddingRight: 5,
                                paddingBottom: 20,
                                paddingLeft: 5,
                                paddingType: "ungrouped",
                                paddingTopSuffix: "px",
                                paddingBottomSuffix: "px",
                                paddingRightSuffix: "px",
                                paddingLeftSuffix: "px",
                                showOnMobile: "on"
                            }
                        }
                    ],
                    bgImageWidth: 0,
                    bgImageHeight: 0,
                    bgImageSrc: "",
                    bgPositionX: 50,
                    bgPositionY: 50,
                    bgPopulation: "",
                    bgColorOpacity: 1,
                    tempBgColorOpacity: 1,
                    bgColorHex: "#ffffff",
                    bgColorPalette: "",
                    paddingType: "grouped",
                    paddingTop: 0,
                    paddingBottom: 0,
                    padding: 0,
                    boxShadow: "on",
                    boxShadowBlur: 15,
                    boxShadowColorOpacity: 0.1000000000000000055511151231257827021181583404541015625,
                    boxShadowVertical: 0,
                    boxShadowColorHex: "#000000",
                    boxShadowColorPalette: ""
                }
            }
        ],
        type: "animated"
    }
}
};