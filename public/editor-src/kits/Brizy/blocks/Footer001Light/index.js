module.exports = {
  id: "Footer001Light",
  thumbnailWidth: 600,
  thumbnailHeight: 311,
  title: "Footer001Light",
  keywords: "footer",
  cat: [12],
  type: 0,
  pro: true,
  resolve: {
    type: "SectionFooter",
    value: {
      _styles: ["sectionFooter"],
      items: [
        {
          type: "Row",
          value: {
            _styles: ["row"],
            items: [
              {
                type: "Column",
                value: {
                  _styles: ["column"],
                  items: []
                }
              },
              {
                type: "Column",
                value: {
                  _styles: ["column"],
                  items: []
                }
              }
            ]
          }
        }
      ]
    }
  }
};
