module.exports = {
id: "Dimi082Light",

thumbnailWidth: 600,
thumbnailHeight: 80,
title: "Dimi082Light",

keywords: "Header",
cat: [21],
type: 0,
pro: true,
resolve: {
    type: "SectionHeader",
    value: {
        _styles: [
            "sectionHeader"
        ],
        items: [
            {
                type: "SectionHeaderItem",
                value: {
                    _styles: [
                        "sectionHeaderItem"
                    ],
                    items: [
                        {
                            type: "Row",
                            value: {
                                _styles: [
                                    "row"
                                ],
                                items: [
                                    {
                                        type: "Column",
                                        value: {
                                            _styles: [
                                                "column"
                                            ],
                                            items: [
                                                {
                                                    type: "Wrapper",
                                                    value: {
                                                        _styles: [
                                                            "wrapper",
                                                            "wrapper--image"
                                                        ],
                                                        items: [
                                                            {
                                                                type: "Image",
                                                                value: {
                                                                    _styles: [
                                                                        "image"
                                                                    ],
                                                                    imageWidth: 2129,
                                                                    imageHeight: 457,
                                                                    imageSrc: "b731902c6bf15805972db395c1ec4670.png",
                                                                    height: 100,
                                                                    positionX: 50,
                                                                    positionY: 50,
                                                                    imagePopulation: "",
                                                                    resize: 85,
                                                                    mobileResize: 40,
                                                                    tabletPositionX: 52,
                                                                    tabletPositionY: 56,
                                                                    tabletHeight: 100,
                                                                    tabletResize: 95
                                                                }
                                                            }
                                                        ],
                                                        horizontalAlign: "left",
                                                        mobileHorizontalAlign: "center"
                                                    }
                                                }
                                            ],
                                            width: 18,
                                            verticalAlign: "center",
                                            paddingTop: 0,
                                            paddingTopSuffix: "px",
                                            padding: 0,
                                            paddingBottom: 0,
                                            paddingBottomSuffix: "px",
                                            paddingType: "grouped",
                                            paddingSuffix: "px",
                                            paddingRight: 0,
                                            paddingLeft: 0,
                                            tabletWidth: 26.39999999999999857891452847979962825775146484375
                                        }
                                    },
                                    {
                                        type: "Column",
                                        value: {
                                            _styles: [
                                                "column"
                                            ],
                                            items: [
                                                {
                                                    type: "Wrapper",
                                                    value: {
                                                        _styles: [
                                                            "wrapper",
                                                            "wrapper--menu"
                                                        ],
                                                        items: [
                                                            {
                                                                type: "Menu",
                                                                value: {
                                                                    _styles: [
                                                                        "menu"
                                                                    ],
                                                                    menuSelected: "c4e7f1ac292ed722e56bcd0c8468972f",
                                                                    fontSize: 18,
                                                                    fontFamily: "roboto",
                                                                    lineHeight: 1.5,
                                                                    letterSpacing: 0,
                                                                    fontWeight: 400,
                                                                    fontStyle: "",
                                                                    itemPadding: 49,
                                                                    itemPaddingRight: 49,
                                                                    itemPaddingLeft: 49,
                                                                    colorPalette: "color1",
                                                                    colorOpacity: 1,
                                                                    hoverColorPalette: "color3",
                                                                    colorHex: "#41444e",
                                                                    tempColorOpacity: 1,
                                                                    hoverColorHex: "#41444e",
                                                                    hoverColorOpacity: 1,
                                                                    mobileMMenu: "on",
                                                                    mobileFontSize: 18,
                                                                    mobileLineHeight: 2,
                                                                    mobileLetterSpacing: 0,
                                                                    mobileFontWeight: 600,
                                                                    mobileFontStyle: "",
                                                                    tabletMMenu: "off",
                                                                    tabletFontSize: 17,
                                                                    tabletLineHeight: 1.8000000000000000444089209850062616169452667236328125,
                                                                    tabletLetterSpacing: 0,
                                                                    tabletFontWeight: 400,
                                                                    tabletFontStyle: "",
                                                                    mobileMMenuSize: 23,
                                                                    mMenuTitle: "Main Menu",
                                                                    mMenu: "off",
                                                                    mMenuSize: 23,
                                                                    mMenuFontSize: 16,
                                                                    mMenuFontFamily: "roboto",
                                                                    mMenuLineHeight: 2.5,
                                                                    mMenuLetterSpacing: 0,
                                                                    mMenuFontWeight: 400,
                                                                    mMenuFontStyle: "",
                                                                    mMenuBorderColorHex: "#ffffff",
                                                                    mMenuBorderColorOpacity: 0.08000000000000000166533453693773481063544750213623046875,
                                                                    mMenuBorderColorPalette: "",
                                                                    mMenuTempBorderColorOpacity: 0.08000000000000000166533453693773481063544750213623046875,
                                                                    mMenuBgColorPalette: "color1",
                                                                    mMenuBgColorOpacity: 1,
                                                                    mMenuBgColorHex: "#191b21",
                                                                    mMenuTempBgColorOpacity: 1,
                                                                    mMenuHoverColorPalette: "color3",
                                                                    mMenuHoverColorOpacity: 1,
                                                                    mMenuItemHorizontalAlign: "center",
                                                                    tabletMMenuFontSize: 16,
                                                                    tabletMMenuLineHeight: 2.5,
                                                                    tabletMMenuLetterSpacing: 0,
                                                                    tabletMMenuFontWeight: 400,
                                                                    tabletMMenuFontStyle: "",
                                                                    mobileMMenuFontSize: 16,
                                                                    mobileMMenuLineHeight: 2.5,
                                                                    mobileMMenuLetterSpacing: 0,
                                                                    mobileMMenuFontWeight: 400,
                                                                    mobileMMenuFontStyle: "",
                                                                    tabletHorizontalAlign: "left",
                                                                    mMenuPosition: "left",
                                                                    tabletMMenuSize: 23,
                                                                    subMenuBgColorHex: "#ffffff",
                                                                    subMenuBgColorOpacity: 1,
                                                                    subMenuBgColorPalette: "",
                                                                    subMenuTempBgColorOpacity: 1,
                                                                    subMenuHoverBgColorHex: "#ffffff",
                                                                    subMenuHoverBgColorOpacity: "#927474",
                                                                    subMenuBorderColorHex: "#000000",
                                                                    subMenuBorderColorOpacity: 0.08000000000000000166533453693773481063544750213623046875,
                                                                    subMenuBorderColorPalette: "",
                                                                    subMenuTempBorderColorOpacity: 0.08000000000000000166533453693773481063544750213623046875,
                                                                    subMenuHoverBorderColorHex: "#000000",
                                                                    subMenuHoverBorderColorOpacity: 0.08000000000000000166533453693773481063544750213623046875,
                                                                    subMenuColorPalette: "color1",
                                                                    subMenuColorOpacity: 1,
                                                                    subMenuHoverColorPalette: "color3",
                                                                    mMenuColorPalette: "",
                                                                    subMenuFontSize: 15,
                                                                    subMenuFontFamily: "roboto",
                                                                    subMenuLineHeight: 1.3000000000000000444089209850062616169452667236328125,
                                                                    subMenuLetterSpacing: 0,
                                                                    subMenuFontWeight: 400,
                                                                    subMenuFontStyle: "",
                                                                    subMenuHoverBorderColorPalette: "",
                                                                    subMenuTempHoverBorderColorOpacity: 0.08000000000000000166533453693773481063544750213623046875,
                                                                    subMenuHoverColorOpacity: 1,
                                                                    mMenuColorHex: "#ffffff",
                                                                    mMenuColorOpacity: 1,
                                                                    mMenuTempColorOpacity: 1,
                                                                    mMenuHoverColorHex: "#ffffff"
                                                                }
                                                            }
                                                        ],
                                                        showOnMobile: "on",
                                                        showOnTablet: "on",
                                                        tabletHorizontalAlign: "right",
                                                        mobileMarginTop: 0,
                                                        mobileMarginTopSuffix: "px",
                                                        mobileMargin: 0
                                                    }
                                                }
                                            ],
                                            verticalAlign: "center",
                                            width: 64,
                                            paddingTop: 0,
                                            paddingTopSuffix: "px",
                                            padding: 15,
                                            paddingBottom: 0,
                                            paddingBottomSuffix: "px",
                                            tabletWidth: 73.099999999999994315658113919198513031005859375
                                        }
                                    },
                                    {
                                        type: "Column",
                                        value: {
                                            _styles: [
                                                "column"
                                            ],
                                            items: [
                                                {
                                                    type: "Wrapper",
                                                    value: {
                                                        _styles: [
                                                            "wrapper",
                                                            "wrapper--menu"
                                                        ],
                                                        items: [
                                                            {
                                                                type: "Menu",
                                                                value: {
                                                                    _styles: [
                                                                        "menu"
                                                                    ],
                                                                    menuSelected: "b3ab7f2f47ac841651948879154bef10",
                                                                    mMenu: "on",
                                                                    horizontalAlign: "right",
                                                                    mMenuSize: 23,
                                                                    mMenuIconColorPalette: "color1",
                                                                    mMenuIconColorHex: "",
                                                                    mMenuIconColorOpacity: 1,
                                                                    mMenuPosition: "right",
                                                                    mMenuItemHorizontalAlign: "center",
                                                                    mMenuFontSize: 16,
                                                                    mMenuFontFamily: "roboto",
                                                                    mMenuLineHeight: 2.5,
                                                                    mMenuLetterSpacing: 0,
                                                                    mMenuFontWeight: 400,
                                                                    mMenuFontStyle: "",
                                                                    mMenuBorderColorHex: "#ffffff",
                                                                    mMenuBorderColorOpacity: 0.08000000000000000166533453693773481063544750213623046875,
                                                                    mMenuBorderColorPalette: "",
                                                                    mMenuTempBorderColorOpacity: 0.08000000000000000166533453693773481063544750213623046875,
                                                                    mMenuTitle: "Secondary Links",
                                                                    mMenuHoverColorPalette: "color3",
                                                                    mMenuHoverColorOpacity: 1,
                                                                    mMenuBgColorPalette: "color1",
                                                                    mMenuBgColorOpacity: 1,
                                                                    tabletHorizontalAlign: "center",
                                                                    mobileHorizontalAlign: "center",
                                                                    tabletMMenuFontSize: 16,
                                                                    tabletMMenuLineHeight: 2.5,
                                                                    tabletMMenuLetterSpacing: 0,
                                                                    tabletMMenuFontWeight: 400,
                                                                    tabletMMenuFontStyle: "",
                                                                    mobileMMenuFontSize: 16,
                                                                    mobileMMenuLineHeight: 2.5,
                                                                    mobileMMenuLetterSpacing: 0,
                                                                    mobileMMenuFontWeight: 400,
                                                                    mobileMMenuFontStyle: ""
                                                                }
                                                            }
                                                        ]
                                                    }
                                                }
                                            ],
                                            verticalAlign: "center",
                                            width: 18,
                                            paddingTop: 0,
                                            paddingTopSuffix: "px",
                                            padding: 15,
                                            paddingBottom: 0,
                                            paddingBottomSuffix: "px",
                                            tabletWidth: 100,
                                            showOnTablet: "off",
                                            showOnMobile: "off"
                                        }
                                    }
                                ]
                            }
                        }
                    ],
                    paddingType: "grouped",
                    paddingTop: 10,
                    paddingBottom: 10,
                    padding: 10,
                    bgColorHex: "#ffffff",
                    bgColorOpacity: 1,
                    bgColorPalette: "",
                    tempBgColorOpacity: 1,
                    tabletPaddingType: "ungrouped",
                    tabletPaddingTop: 15,
                    tabletPaddingBottom: 15,
                    tabletPadding: 50,
                    mobilePaddingType: "ungrouped",
                    mobilePaddingTop: 15,
                    mobilePaddingBottom: 15,
                    mobilePadding: 25,
                    containerType: "boxed",
                    mobileBgColorHex: "#ffffff",
                    mobileBgColorOpacity: 1,
                    mobileBgColorPalette: "",
                    boxShadow: "on",
                    boxShadowBlur: 6,
                    boxShadowColorOpacity: 0.1000000000000000055511151231257827021181583404541015625,
                    boxShadowColorHex: "#000000",
                    boxShadowColorPalette: "",
                    boxShadowVertical: 0
                }
            },
            {
                type: "SectionHeaderStickyItem",
                value: {
                    _styles: [
                        "sectionHeaderStickyItem"
                    ],
                    items: [],
                    bgImageWidth: 0,
                    bgImageHeight: 0,
                    bgImageSrc: "",
                    bgPositionX: 50,
                    bgPositionY: 50,
                    bgPopulation: "",
                    bgColorOpacity: 1,
                    tempBgColorOpacity: 1,
                    bgColorHex: "#ffffff",
                    bgColorPalette: ""
                }
            }
        ],
        type: "fixed"
    }
}
};