module.exports = {
id: "Gabi062Dark",

thumbnailWidth: 600,
thumbnailHeight: 267,
title: "Gabi062Dark",

keywords: "Post, Blog, Content",
cat: [17, 18, 20],
type: 1,
pro: true,
resolve: {
    type: "Section",
    value: {
        _styles: [
            "section"
        ],
        items: [
            {
                type: "SectionItem",
                value: {
                    _styles: [
                        "section-item"
                    ],
                    items: [
                        {
                            type: "Wrapper",
                            value: {
                                _styles: [
                                    "wrapper",
                                    "wrapper--richText"
                                ],
                                items: [
                                    {
                                        type: "RichText",
                                        value: {
                                            _styles: [
                                                "richText"
                                            ],
                                            text: "<p class=\"brz-text-lg-center brz-tp-heading5\"><span style=\"color: rgb(255, 255, 255);\">OTHER POPULAR ARTICLES</span></p>"
                                        }
                                    }
                                ]
                            }
                        },
                        {
                            type: "Row",
                            value: {
                                _styles: [
                                    "row"
                                ],
                                items: [
                                    {
                                        type: "Column",
                                        value: {
                                            _styles: [
                                                "column"
                                            ],
                                            items: [
                                                {
                                                    type: "Cloneable",
                                                    value: {
                                                        _styles: [
                                                            "wrapper-clone",
                                                            "wrapper-clone--button"
                                                        ],
                                                        items: [
                                                            {
                                                                type: "Button",
                                                                value: {
                                                                    _styles: [
                                                                        "button"
                                                                    ],
                                                                    size: "custom",
                                                                    mobileSize: "custom",
                                                                    fontSize: 11,
                                                                    paddingTB: 1,
                                                                    paddingTop: 1,
                                                                    paddingBottom: 1,
                                                                    tempPaddingTB: 1,
                                                                    tempPaddingTop: 1,
                                                                    tempPaddingBottom: 1,
                                                                    paddingRL: 9,
                                                                    paddingRight: 9,
                                                                    paddingLeft: 9,
                                                                    tempPaddingRL: 9,
                                                                    tempPaddingRight: 9,
                                                                    tempPaddingLeft: 9,
                                                                    borderRadius: 5,
                                                                    borderWidth: 2,
                                                                    mobilePaddingTop: 11,
                                                                    mobilePaddingRight: 26,
                                                                    mobilePaddingBottom: 11,
                                                                    mobilePaddingLeft: 26,
                                                                    borderRadiusType: "custom",
                                                                    tempBorderRadiusType: "custom",
                                                                    fillType: "outline",
                                                                    borderColorOpacity: 1,
                                                                    borderColorPalette: "",
                                                                    bgColorOpacity: 0,
                                                                    bgColorPalette: "",
                                                                    hoverBgColorOpacity: 0.9899999999999999911182158029987476766109466552734375,
                                                                    hoverBorderColorOpacity: 1,
                                                                    tempBorderRadius: 5,
                                                                    tempMobileBorderRadius: 5,
                                                                    tempFillType: "outline",
                                                                    mobilePaddingRL: 26,
                                                                    iconName: "",
                                                                    iconType: "",
                                                                    fontFamily: "montserrat",
                                                                    lineHeight: 1.8000000000000000444089209850062616169452667236328125,
                                                                    letterSpacing: 1,
                                                                    fontWeight: 700,
                                                                    fontStyle: "",
                                                                    text: "NATURE",
                                                                    borderColorHex: "#ffffff",
                                                                    tempBorderColorOpacity: 1,
                                                                    tempBorderColorPalette: "",
                                                                    hoverBgColorHex: "#ffffff",
                                                                    tempBorderWidth: 2,
                                                                    hoverBorderColorHex: "#239ddb",
                                                                    hoverBorderColorPalette: "color3",
                                                                    hoverColorHex: "#ffffff",
                                                                    hoverColorOpacity: 1,
                                                                    hoverColorPalette: "color3",
                                                                    tempHoverBgColorOpacity: 0.8000000000000000444089209850062616169452667236328125,
                                                                    hoverBgColorPalette: "",
                                                                    tempHoverBgColorPalette: "",
                                                                    tempHoverBorderColorPalette: "color3",
                                                                    colorPalette: "",
                                                                    colorOpacity: 1,
                                                                    colorHex: "#ffffff",
                                                                    tempColorOpacity: 1
                                                                }
                                                            }
                                                        ],
                                                        horizontalAlign: "center"
                                                    }
                                                },
                                                {
                                                    type: "Wrapper",
                                                    value: {
                                                        _styles: [
                                                            "wrapper",
                                                            "wrapper--richText"
                                                        ],
                                                        items: [
                                                            {
                                                                type: "RichText",
                                                                value: {
                                                                    _styles: [
                                                                        "richText"
                                                                    ],
                                                                    text: "<p class=\"brz-text-lg-center brz-tp-heading5\"><span style=\"color: rgb(255, 255, 255);\">Lorem Ipsum Dolor Sit Amet Consectetur</span></p>"
                                                                }
                                                            }
                                                        ]
                                                    }
                                                },
                                                {
                                                    type: "Wrapper",
                                                    value: {
                                                        _styles: [
                                                            "wrapper",
                                                            "wrapper--richText"
                                                        ],
                                                        items: [
                                                            {
                                                                type: "RichText",
                                                                value: {
                                                                    _styles: [
                                                                        "richText"
                                                                    ],
                                                                    text: "<p class=\"brz-text-lg-center brz-mt-lg-0 brz-tp-paragraph\"><span style=\"color: rgb(255, 255, 255); opacity: 0.68;\"> The Kurykans, a Siberian tribe who inhabited the area in the sixth century, gave it a name that translates to \u201cmuch water\u201d.</span></p>"
                                                                }
                                                            }
                                                        ]
                                                    }
                                                },
                                                {
                                                    type: "Cloneable",
                                                    value: {
                                                        _styles: [
                                                            "wrapper-clone",
                                                            "wrapper-clone--button"
                                                        ],
                                                        items: [
                                                            {
                                                                type: "Button",
                                                                value: {
                                                                    _styles: [
                                                                        "button"
                                                                    ],
                                                                    size: "custom",
                                                                    mobileSize: "custom",
                                                                    fontSize: 11,
                                                                    paddingTB: 6,
                                                                    paddingTop: 6,
                                                                    paddingBottom: 6,
                                                                    tempPaddingTB: 6,
                                                                    tempPaddingTop: 6,
                                                                    tempPaddingBottom: 6,
                                                                    paddingRL: 26,
                                                                    paddingRight: 26,
                                                                    paddingLeft: 26,
                                                                    tempPaddingRL: 26,
                                                                    tempPaddingRight: 26,
                                                                    tempPaddingLeft: 26,
                                                                    borderRadius: 18,
                                                                    borderWidth: 2,
                                                                    mobilePaddingTop: 11,
                                                                    mobilePaddingRight: 26,
                                                                    mobilePaddingBottom: 11,
                                                                    mobilePaddingLeft: 26,
                                                                    mobileBorderRadius: 24,
                                                                    borderRadiusType: "rounded",
                                                                    tempBorderRadiusType: "rounded",
                                                                    fillType: "outline",
                                                                    borderColorOpacity: 0.460000000000000019984014443252817727625370025634765625,
                                                                    borderColorPalette: "",
                                                                    bgColorOpacity: 0,
                                                                    bgColorPalette: "",
                                                                    hoverBgColorOpacity: 1,
                                                                    hoverBorderColorOpacity: 0,
                                                                    text: "READ MORE",
                                                                    fontFamily: "montserrat",
                                                                    lineHeight: 1.8000000000000000444089209850062616169452667236328125,
                                                                    letterSpacing: 3.5,
                                                                    fontWeight: 600,
                                                                    fontStyle: "button",
                                                                    iconName: "",
                                                                    iconType: "",
                                                                    iconSize: "custom",
                                                                    iconCustomSize: 13,
                                                                    iconPosition: "left",
                                                                    iconSpacing: 24,
                                                                    bgColorHex: "#00adf2",
                                                                    tempBorderWidth: 2,
                                                                    tempBorderRadius: 19,
                                                                    tempMobileBorderRadius: 19,
                                                                    hoverBgColorPalette: "color2",
                                                                    hoverBgColorHex: "#0099d5",
                                                                    tempHoverBgColorOpacity: 1,
                                                                    tempHoverBgColorPalette: "",
                                                                    hoverBorderColorHex: "#00adf2",
                                                                    hoverBorderColorPalette: "color3",
                                                                    tempHoverBorderColorPalette: "color3",
                                                                    colorPalette: "",
                                                                    colorOpacity: 1,
                                                                    tempBgColorPalette: "color3",
                                                                    tempBgColorOpacity: 1,
                                                                    borderColorHex: "#ffffff",
                                                                    tempBorderColorPalette: "",
                                                                    tempFillType: "outline",
                                                                    mobilePaddingRL: 26,
                                                                    hoverColorHex: "#ffffff",
                                                                    hoverColorOpacity: 1,
                                                                    hoverColorPalette: "",
                                                                    tempBorderColorOpacity: 0.460000000000000019984014443252817727625370025634765625,
                                                                    colorHex: "#ffffff",
                                                                    tempColorOpacity: 1,
                                                                    mobileFontStyle: "button"
                                                                }
                                                            }
                                                        ],
                                                        horizontalAlign: "center",
                                                        itemPadding: 17,
                                                        itemPaddingRight: 17,
                                                        itemPaddingLeft: 17
                                                    }
                                                }
                                            ],
                                            width: 33.2999999999999971578290569595992565155029296875,
                                            bgColorPalette: "color3",
                                            bgColorHex: "",
                                            bgColorOpacity: 1,
                                            mobilePaddingRight: 10,
                                            mobilePaddingLeft: 10,
                                            borderRadius: 6,
                                            borderTopLeftRadius: 6,
                                            borderTopRightRadius: 6,
                                            borderBottomLeftRadius: 6,
                                            borderBottomRightRadius: 6,
                                            tempBorderRadius: 0,
                                            tempBorderTopLeftRadius: 0,
                                            tempBorderTopRightRadius: 0,
                                            tempBorderBottomRightRadius: 0,
                                            tempBorderBottomLeftRadius: 0,
                                            margin: 23,
                                            marginSuffix: "px",
                                            marginTop: 23,
                                            marginRight: 23,
                                            marginBottom: 23,
                                            marginLeft: 23,
                                            paddingTop: 48,
                                            paddingTopSuffix: "px",
                                            padding: 48,
                                            paddingType: "grouped",
                                            paddingSuffix: "px",
                                            paddingRight: 48,
                                            paddingBottom: 48,
                                            paddingLeft: 48,
                                            verticalAlign: "center"
                                        }
                                    },
                                    {
                                        type: "Column",
                                        value: {
                                            _styles: [
                                                "column"
                                            ],
                                            items: [
                                                {
                                                    type: "Cloneable",
                                                    value: {
                                                        _styles: [
                                                            "wrapper-clone",
                                                            "wrapper-clone--button"
                                                        ],
                                                        items: [
                                                            {
                                                                type: "Button",
                                                                value: {
                                                                    _styles: [
                                                                        "button"
                                                                    ],
                                                                    size: "custom",
                                                                    mobileSize: "custom",
                                                                    fontSize: 11,
                                                                    paddingTB: 1,
                                                                    paddingTop: 1,
                                                                    paddingBottom: 1,
                                                                    tempPaddingTB: 1,
                                                                    tempPaddingTop: 1,
                                                                    tempPaddingBottom: 1,
                                                                    paddingRL: 9,
                                                                    paddingRight: 9,
                                                                    paddingLeft: 9,
                                                                    tempPaddingRL: 9,
                                                                    tempPaddingRight: 9,
                                                                    tempPaddingLeft: 9,
                                                                    borderRadius: 5,
                                                                    borderWidth: 2,
                                                                    mobilePaddingTop: 11,
                                                                    mobilePaddingRight: 26,
                                                                    mobilePaddingBottom: 11,
                                                                    mobilePaddingLeft: 26,
                                                                    borderRadiusType: "custom",
                                                                    tempBorderRadiusType: "custom",
                                                                    fillType: "outline",
                                                                    borderColorOpacity: 1,
                                                                    borderColorPalette: "color2",
                                                                    bgColorOpacity: 0,
                                                                    bgColorPalette: "",
                                                                    hoverBgColorOpacity: 0.9899999999999999911182158029987476766109466552734375,
                                                                    hoverBorderColorOpacity: 1,
                                                                    tempBorderRadius: 5,
                                                                    tempMobileBorderRadius: 5,
                                                                    tempFillType: "outline",
                                                                    mobilePaddingRL: 26,
                                                                    iconName: "",
                                                                    iconType: "",
                                                                    fontFamily: "montserrat",
                                                                    lineHeight: 1.8000000000000000444089209850062616169452667236328125,
                                                                    letterSpacing: 1,
                                                                    fontWeight: 700,
                                                                    fontStyle: "",
                                                                    text: "TRENDS",
                                                                    borderColorHex: "#ffffff",
                                                                    tempBorderColorOpacity: 1,
                                                                    tempBorderColorPalette: "color2",
                                                                    hoverBgColorHex: "#ffffff",
                                                                    tempBorderWidth: 2,
                                                                    hoverBorderColorHex: "#239ddb",
                                                                    hoverBorderColorPalette: "color3",
                                                                    hoverColorHex: "#ffffff",
                                                                    hoverColorOpacity: 1,
                                                                    hoverColorPalette: "color3",
                                                                    tempHoverBgColorOpacity: 0.8000000000000000444089209850062616169452667236328125,
                                                                    hoverBgColorPalette: "",
                                                                    tempHoverBgColorPalette: "",
                                                                    tempHoverBorderColorPalette: "color3",
                                                                    colorPalette: "color2",
                                                                    colorOpacity: 1,
                                                                    colorHex: "#ffffff",
                                                                    tempColorOpacity: 1
                                                                }
                                                            }
                                                        ],
                                                        horizontalAlign: "center"
                                                    }
                                                },
                                                {
                                                    type: "Wrapper",
                                                    value: {
                                                        _styles: [
                                                            "wrapper",
                                                            "wrapper--richText"
                                                        ],
                                                        items: [
                                                            {
                                                                type: "RichText",
                                                                value: {
                                                                    _styles: [
                                                                        "richText"
                                                                    ],
                                                                    text: "<p class=\"brz-text-lg-center brz-tp-heading5\"><span class=\"brz-cp-color2\">Best 5 examples of Digital Photography of the modern world</span></p>"
                                                                }
                                                            }
                                                        ]
                                                    }
                                                },
                                                {
                                                    type: "Wrapper",
                                                    value: {
                                                        _styles: [
                                                            "wrapper",
                                                            "wrapper--richText"
                                                        ],
                                                        items: [
                                                            {
                                                                type: "RichText",
                                                                value: {
                                                                    _styles: [
                                                                        "richText"
                                                                    ],
                                                                    text: "<p class=\"brz-mt-lg-0 brz-text-lg-center brz-tp-paragraph\"><span style=\"opacity: 0.5;\" class=\"brz-cp-color2\"> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ali.</span></p>"
                                                                }
                                                            }
                                                        ]
                                                    }
                                                },
                                                {
                                                    type: "Cloneable",
                                                    value: {
                                                        _styles: [
                                                            "wrapper-clone",
                                                            "wrapper-clone--button"
                                                        ],
                                                        items: [
                                                            {
                                                                type: "Button",
                                                                value: {
                                                                    _styles: [
                                                                        "button"
                                                                    ],
                                                                    size: "custom",
                                                                    mobileSize: "custom",
                                                                    fontSize: 11,
                                                                    paddingTB: 6,
                                                                    paddingTop: 6,
                                                                    paddingBottom: 6,
                                                                    tempPaddingTB: 6,
                                                                    tempPaddingTop: 6,
                                                                    tempPaddingBottom: 6,
                                                                    paddingRL: 26,
                                                                    paddingRight: 26,
                                                                    paddingLeft: 26,
                                                                    tempPaddingRL: 26,
                                                                    tempPaddingRight: 26,
                                                                    tempPaddingLeft: 26,
                                                                    borderRadius: 18,
                                                                    borderWidth: 2,
                                                                    mobilePaddingTop: 11,
                                                                    mobilePaddingRight: 26,
                                                                    mobilePaddingBottom: 11,
                                                                    mobilePaddingLeft: 26,
                                                                    mobileBorderRadius: 24,
                                                                    borderRadiusType: "rounded",
                                                                    tempBorderRadiusType: "rounded",
                                                                    fillType: "outline",
                                                                    borderColorOpacity: 0.57999999999999996003197111349436454474925994873046875,
                                                                    borderColorPalette: "color2",
                                                                    bgColorOpacity: 0,
                                                                    bgColorPalette: "",
                                                                    hoverBgColorOpacity: 1,
                                                                    hoverBorderColorOpacity: 0,
                                                                    text: "READ MORE",
                                                                    fontFamily: "montserrat",
                                                                    lineHeight: 1.8000000000000000444089209850062616169452667236328125,
                                                                    letterSpacing: 3.5,
                                                                    fontWeight: 600,
                                                                    fontStyle: "button",
                                                                    iconName: "",
                                                                    iconType: "",
                                                                    iconSize: "custom",
                                                                    iconCustomSize: 13,
                                                                    iconPosition: "left",
                                                                    iconSpacing: 24,
                                                                    bgColorHex: "#00adf2",
                                                                    tempBorderWidth: 2,
                                                                    tempBorderRadius: 19,
                                                                    tempMobileBorderRadius: 19,
                                                                    hoverBgColorPalette: "color2",
                                                                    hoverBgColorHex: "#0099d5",
                                                                    tempHoverBgColorOpacity: 1,
                                                                    tempHoverBgColorPalette: "",
                                                                    hoverBorderColorHex: "#00adf2",
                                                                    hoverBorderColorPalette: "color3",
                                                                    tempHoverBorderColorPalette: "color3",
                                                                    colorPalette: "color2",
                                                                    colorOpacity: 1,
                                                                    tempBgColorPalette: "color3",
                                                                    tempBgColorOpacity: 1,
                                                                    borderColorHex: "#142850",
                                                                    tempBorderColorPalette: "color2",
                                                                    tempFillType: "outline",
                                                                    mobilePaddingRL: 26,
                                                                    hoverColorHex: "#ffffff",
                                                                    hoverColorOpacity: 1,
                                                                    hoverColorPalette: "",
                                                                    tempBorderColorOpacity: 0.57999999999999996003197111349436454474925994873046875,
                                                                    colorHex: "#ffffff",
                                                                    tempColorOpacity: 1,
                                                                    mobileFontStyle: "button"
                                                                }
                                                            }
                                                        ],
                                                        horizontalAlign: "center",
                                                        itemPadding: 17,
                                                        itemPaddingRight: 17,
                                                        itemPaddingLeft: 17
                                                    }
                                                }
                                            ],
                                            width: 33.2999999999999971578290569595992565155029296875,
                                            bgColorPalette: "color5",
                                            bgColorHex: "",
                                            bgColorOpacity: 1,
                                            mobilePaddingRight: 10,
                                            mobilePaddingLeft: 10,
                                            borderRadius: 6,
                                            borderTopLeftRadius: 6,
                                            borderTopRightRadius: 6,
                                            borderBottomLeftRadius: 6,
                                            borderBottomRightRadius: 6,
                                            tempBorderRadius: 0,
                                            tempBorderTopLeftRadius: 0,
                                            tempBorderTopRightRadius: 0,
                                            tempBorderBottomRightRadius: 0,
                                            tempBorderBottomLeftRadius: 0,
                                            margin: 23,
                                            marginSuffix: "px",
                                            marginTop: 23,
                                            marginRight: 23,
                                            marginBottom: 23,
                                            marginLeft: 23,
                                            paddingTop: 48,
                                            paddingTopSuffix: "px",
                                            padding: 48,
                                            paddingType: "grouped",
                                            paddingSuffix: "px",
                                            paddingRight: 48,
                                            paddingBottom: 48,
                                            paddingLeft: 48,
                                            verticalAlign: "center"
                                        }
                                    },
                                    {
                                        type: "Column",
                                        value: {
                                            _styles: [
                                                "column"
                                            ],
                                            items: [
                                                {
                                                    type: "Cloneable",
                                                    value: {
                                                        _styles: [
                                                            "wrapper-clone",
                                                            "wrapper-clone--button"
                                                        ],
                                                        items: [
                                                            {
                                                                type: "Button",
                                                                value: {
                                                                    _styles: [
                                                                        "button"
                                                                    ],
                                                                    size: "custom",
                                                                    mobileSize: "custom",
                                                                    fontSize: 11,
                                                                    paddingTB: 1,
                                                                    paddingTop: 1,
                                                                    paddingBottom: 1,
                                                                    tempPaddingTB: 1,
                                                                    tempPaddingTop: 1,
                                                                    tempPaddingBottom: 1,
                                                                    paddingRL: 9,
                                                                    paddingRight: 9,
                                                                    paddingLeft: 9,
                                                                    tempPaddingRL: 9,
                                                                    tempPaddingRight: 9,
                                                                    tempPaddingLeft: 9,
                                                                    borderRadius: 5,
                                                                    borderWidth: 2,
                                                                    mobilePaddingTop: 11,
                                                                    mobilePaddingRight: 26,
                                                                    mobilePaddingBottom: 11,
                                                                    mobilePaddingLeft: 26,
                                                                    borderRadiusType: "custom",
                                                                    tempBorderRadiusType: "custom",
                                                                    fillType: "outline",
                                                                    borderColorOpacity: 1,
                                                                    borderColorPalette: "",
                                                                    bgColorOpacity: 0,
                                                                    bgColorPalette: "",
                                                                    hoverBgColorOpacity: 0.9899999999999999911182158029987476766109466552734375,
                                                                    hoverBorderColorOpacity: 1,
                                                                    tempBorderRadius: 5,
                                                                    tempMobileBorderRadius: 5,
                                                                    tempFillType: "outline",
                                                                    mobilePaddingRL: 26,
                                                                    iconName: "",
                                                                    iconType: "",
                                                                    fontFamily: "montserrat",
                                                                    lineHeight: 1.8000000000000000444089209850062616169452667236328125,
                                                                    letterSpacing: 1,
                                                                    fontWeight: 700,
                                                                    fontStyle: "",
                                                                    text: "LIFESTYLE",
                                                                    borderColorHex: "#ffffff",
                                                                    tempBorderColorOpacity: 1,
                                                                    tempBorderColorPalette: "",
                                                                    hoverBgColorHex: "#ffffff",
                                                                    tempBorderWidth: 2,
                                                                    hoverBorderColorHex: "#239ddb",
                                                                    hoverBorderColorPalette: "color3",
                                                                    hoverColorHex: "#ffffff",
                                                                    hoverColorOpacity: 1,
                                                                    hoverColorPalette: "color3",
                                                                    tempHoverBgColorOpacity: 0.8000000000000000444089209850062616169452667236328125,
                                                                    hoverBgColorPalette: "",
                                                                    tempHoverBgColorPalette: "",
                                                                    tempHoverBorderColorPalette: "color3",
                                                                    colorPalette: "",
                                                                    colorOpacity: 1,
                                                                    colorHex: "#ffffff",
                                                                    tempColorOpacity: 1
                                                                }
                                                            }
                                                        ],
                                                        horizontalAlign: "center"
                                                    }
                                                },
                                                {
                                                    type: "Wrapper",
                                                    value: {
                                                        _styles: [
                                                            "wrapper",
                                                            "wrapper--richText"
                                                        ],
                                                        items: [
                                                            {
                                                                type: "RichText",
                                                                value: {
                                                                    _styles: [
                                                                        "richText"
                                                                    ],
                                                                    text: "<p class=\"brz-text-lg-center brz-tp-heading5\"><span style=\"color: rgb(255, 255, 255);\">Lorem Ipsum Dolor Sit Amet Consectetur</span></p>"
                                                                }
                                                            }
                                                        ]
                                                    }
                                                },
                                                {
                                                    type: "Wrapper",
                                                    value: {
                                                        _styles: [
                                                            "wrapper",
                                                            "wrapper--richText"
                                                        ],
                                                        items: [
                                                            {
                                                                type: "RichText",
                                                                value: {
                                                                    _styles: [
                                                                        "richText"
                                                                    ],
                                                                    text: "<p class=\"brz-mt-lg-0 brz-text-lg-center brz-tp-paragraph\"><span style=\"opacity: 0.68; color: rgb(255, 255, 255);\"> The Kurykans, a Siberian tribe who inhabited the area in the sixth century, gave it a name that translates to \u201cmuch water\u201d.</span></p>"
                                                                }
                                                            }
                                                        ]
                                                    }
                                                },
                                                {
                                                    type: "Cloneable",
                                                    value: {
                                                        _styles: [
                                                            "wrapper-clone",
                                                            "wrapper-clone--button"
                                                        ],
                                                        items: [
                                                            {
                                                                type: "Button",
                                                                value: {
                                                                    _styles: [
                                                                        "button"
                                                                    ],
                                                                    size: "custom",
                                                                    mobileSize: "custom",
                                                                    fontSize: 11,
                                                                    paddingTB: 6,
                                                                    paddingTop: 6,
                                                                    paddingBottom: 6,
                                                                    tempPaddingTB: 6,
                                                                    tempPaddingTop: 6,
                                                                    tempPaddingBottom: 6,
                                                                    paddingRL: 26,
                                                                    paddingRight: 26,
                                                                    paddingLeft: 26,
                                                                    tempPaddingRL: 26,
                                                                    tempPaddingRight: 26,
                                                                    tempPaddingLeft: 26,
                                                                    borderRadius: 18,
                                                                    borderWidth: 2,
                                                                    mobilePaddingTop: 11,
                                                                    mobilePaddingRight: 26,
                                                                    mobilePaddingBottom: 11,
                                                                    mobilePaddingLeft: 26,
                                                                    mobileBorderRadius: 24,
                                                                    borderRadiusType: "rounded",
                                                                    tempBorderRadiusType: "rounded",
                                                                    fillType: "outline",
                                                                    borderColorOpacity: 0.460000000000000019984014443252817727625370025634765625,
                                                                    borderColorPalette: "",
                                                                    bgColorOpacity: 0,
                                                                    bgColorPalette: "",
                                                                    hoverBgColorOpacity: 1,
                                                                    hoverBorderColorOpacity: 0,
                                                                    text: "READ MORE",
                                                                    fontFamily: "montserrat",
                                                                    lineHeight: 1.8000000000000000444089209850062616169452667236328125,
                                                                    letterSpacing: 3.5,
                                                                    fontWeight: 600,
                                                                    fontStyle: "button",
                                                                    iconName: "",
                                                                    iconType: "",
                                                                    iconSize: "custom",
                                                                    iconCustomSize: 13,
                                                                    iconPosition: "left",
                                                                    iconSpacing: 24,
                                                                    bgColorHex: "#00adf2",
                                                                    tempBorderWidth: 2,
                                                                    tempBorderRadius: 19,
                                                                    tempMobileBorderRadius: 19,
                                                                    hoverBgColorPalette: "color2",
                                                                    hoverBgColorHex: "#0099d5",
                                                                    tempHoverBgColorOpacity: 1,
                                                                    tempHoverBgColorPalette: "",
                                                                    hoverBorderColorHex: "#00adf2",
                                                                    hoverBorderColorPalette: "color3",
                                                                    tempHoverBorderColorPalette: "color3",
                                                                    colorPalette: "",
                                                                    colorOpacity: 1,
                                                                    tempBgColorPalette: "color3",
                                                                    tempBgColorOpacity: 1,
                                                                    borderColorHex: "#ffffff",
                                                                    tempBorderColorPalette: "",
                                                                    tempFillType: "outline",
                                                                    mobilePaddingRL: 26,
                                                                    hoverColorHex: "#ffffff",
                                                                    hoverColorOpacity: 1,
                                                                    hoverColorPalette: "",
                                                                    tempBorderColorOpacity: 0.460000000000000019984014443252817727625370025634765625,
                                                                    colorHex: "#ffffff",
                                                                    tempColorOpacity: 1,
                                                                    mobileFontStyle: "button"
                                                                }
                                                            }
                                                        ],
                                                        horizontalAlign: "center",
                                                        itemPadding: 17,
                                                        itemPaddingRight: 17,
                                                        itemPaddingLeft: 17
                                                    }
                                                }
                                            ],
                                            width: 33.39999999999999857891452847979962825775146484375,
                                            bgColorPalette: "color3",
                                            bgColorHex: "",
                                            bgColorOpacity: 1,
                                            mobilePaddingRight: 10,
                                            mobilePaddingLeft: 10,
                                            borderRadius: 6,
                                            borderTopLeftRadius: 6,
                                            borderTopRightRadius: 6,
                                            borderBottomLeftRadius: 6,
                                            borderBottomRightRadius: 6,
                                            tempBorderRadius: 0,
                                            tempBorderTopLeftRadius: 0,
                                            tempBorderTopRightRadius: 0,
                                            tempBorderBottomRightRadius: 0,
                                            tempBorderBottomLeftRadius: 0,
                                            margin: 23,
                                            marginSuffix: "px",
                                            marginTop: 23,
                                            marginRight: 23,
                                            marginBottom: 23,
                                            marginLeft: 23,
                                            paddingTop: 48,
                                            paddingTopSuffix: "px",
                                            padding: 48,
                                            paddingType: "grouped",
                                            paddingSuffix: "px",
                                            paddingRight: 48,
                                            paddingBottom: 48,
                                            paddingLeft: 48,
                                            verticalAlign: "center"
                                        }
                                    }
                                ]
                            }
                        }
                    ],
                    bgColorPalette: "color1",
                    bgColorHex: "",
                    bgColorOpacity: 1,
                    paddingType: "ungrouped",
                    paddingTop: 70,
                    paddingBottom: 70,
                    padding: 75
                }
            }
        ]
    }
}
};