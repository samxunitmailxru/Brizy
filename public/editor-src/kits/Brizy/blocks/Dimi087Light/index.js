module.exports = {
id: "Dimi087Light",

thumbnailWidth: 750,
thumbnailHeight: 138,
title: "Dimi087Light",

keywords: "Header",
cat: [21],
type: 0,
pro: true,
resolve: {
    type: "SectionHeader",
    value: {
        _styles: [
            "sectionHeader"
        ],
        items: [
            {
                type: "SectionHeaderItem",
                value: {
                    _styles: [
                        "sectionHeaderItem"
                    ],
                    items: [
                        {
                            type: "Row",
                            value: {
                                _styles: [
                                    "row"
                                ],
                                items: [
                                    {
                                        type: "Column",
                                        value: {
                                            _styles: [
                                                "column"
                                            ],
                                            items: [
                                                {
                                                    type: "Wrapper",
                                                    value: {
                                                        _styles: [
                                                            "wrapper",
                                                            "wrapper--menu"
                                                        ],
                                                        items: [
                                                            {
                                                                type: "Menu",
                                                                value: {
                                                                    _styles: [
                                                                        "menu"
                                                                    ],
                                                                    menuSelected: "5959c8793eb7cb49315af570c6bb262a",
                                                                    fontSize: 15,
                                                                    fontFamily: "josefin_sans",
                                                                    lineHeight: 1.6999999999999999555910790149937383830547332763671875,
                                                                    letterSpacing: 1,
                                                                    fontWeight: 400,
                                                                    fontStyle: "",
                                                                    itemPadding: 50,
                                                                    itemPaddingRight: 50,
                                                                    itemPaddingLeft: 50,
                                                                    horizontalAlign: "center",
                                                                    colorHex: "#4c4c4c",
                                                                    colorOpacity: 1,
                                                                    colorPalette: "color1",
                                                                    tempColorOpacity: 1,
                                                                    hoverColorHex: "#4c4c4c",
                                                                    hoverColorOpacity: 1,
                                                                    hoverColorPalette: "color3",
                                                                    subMenuBgColorPalette: "",
                                                                    subMenuBgColorOpacity: 1,
                                                                    subMenuHoverBgColorPalette: "",
                                                                    mMenuBgColorPalette: "",
                                                                    subMenuBorderColorHex: "#000000",
                                                                    subMenuBorderColorOpacity: 0.08000000000000000166533453693773481063544750213623046875,
                                                                    subMenuBorderColorPalette: "",
                                                                    subMenuTempBorderColorOpacity: 0.08000000000000000166533453693773481063544750213623046875,
                                                                    subMenuHoverBorderColorHex: "#000000",
                                                                    subMenuHoverBorderColorOpacity: 0.070000000000000006661338147750939242541790008544921875,
                                                                    mMenuBorderColorHex: "#000000",
                                                                    mMenuBorderColorOpacity: 0.08000000000000000166533453693773481063544750213623046875,
                                                                    subMenuColorHex: "#8b8b8b",
                                                                    subMenuColorOpacity: 1,
                                                                    subMenuColorPalette: "",
                                                                    subMenuTempColorOpacity: 1,
                                                                    subMenuHoverColorHex: "#8b8b8b",
                                                                    subMenuHoverColorOpacity: "#8b8b8b",
                                                                    mMenuColorHex: "#000000",
                                                                    mMenuColorOpacity: 1,
                                                                    mMenuHoverColorHex: "#000000",
                                                                    mMenuHoverColorOpacity: 1,
                                                                    subMenuHoverColorPalette: "color1",
                                                                    mMenuHoverColorPalette: "color3",
                                                                    subMenuHoverBorderColorPalette: "",
                                                                    subMenuTempHoverBorderColorOpacity: 0.070000000000000006661338147750939242541790008544921875,
                                                                    subMenuHoverBgColorHex: "#ffffff",
                                                                    subMenuHoverBgColorOpacity: 1,
                                                                    tabletMMenuSize: 23,
                                                                    tabletHorizontalAlign: "left",
                                                                    tabletMMenuFontSize: 15,
                                                                    tabletMMenuLineHeight: 2,
                                                                    tabletMMenuLetterSpacing: 1,
                                                                    tabletMMenuFontWeight: 400,
                                                                    tabletMMenuFontStyle: "",
                                                                    mMenuBgColorHex: "#ffffff",
                                                                    mMenuBgColorOpacity: 1,
                                                                    mMenuTempBgColorOpacity: 1,
                                                                    mMenuBorderColorPalette: "",
                                                                    mMenuTempBorderColorOpacity: 0.08000000000000000166533453693773481063544750213623046875,
                                                                    mMenuTitle: "Main Menu",
                                                                    mobileHorizontalAlign: "center",
                                                                    mobileMMenuSize: 23,
                                                                    mMenuColorPalette: "color1",
                                                                    mMenuTempColorOpacity: 1,
                                                                    mobileMMenuFontSize: 15,
                                                                    mobileMMenuLineHeight: 2,
                                                                    mobileMMenuLetterSpacing: 1,
                                                                    mobileMMenuFontWeight: 400,
                                                                    mobileMMenuFontStyle: "",
                                                                    tabletMMenuIconColorHex: "",
                                                                    tabletMMenuIconColorOpacity: 1,
                                                                    tabletMMenuIconColorPalette: "color1",
                                                                    mMenu: "off",
                                                                    mMenuSize: 25,
                                                                    subMenuBgColorHex: "#ffffff",
                                                                    subMenuTempBgColorOpacity: 1,
                                                                    subMenuFontSize: 17,
                                                                    subMenuFontFamily: "josefin_sans",
                                                                    subMenuLineHeight: 1,
                                                                    subMenuLetterSpacing: 0,
                                                                    subMenuFontWeight: 400,
                                                                    subMenuFontStyle: "",
                                                                    mMenuIconColorPalette: "color1",
                                                                    mMenuIconColorHex: "",
                                                                    mMenuIconColorOpacity: 1,
                                                                    mMenuFontSize: 15,
                                                                    mMenuFontFamily: "josefin_sans",
                                                                    mMenuLineHeight: 2,
                                                                    mMenuLetterSpacing: 1,
                                                                    mMenuFontWeight: 400,
                                                                    mMenuFontStyle: "",
                                                                    tabletMMenu: "on",
                                                                    tabletFontSize: 13,
                                                                    tabletLineHeight: 2.29999999999999982236431605997495353221893310546875,
                                                                    tabletLetterSpacing: 1,
                                                                    tabletFontWeight: 600,
                                                                    tabletFontStyle: "",
                                                                    tabletItemPadding: 45,
                                                                    tabletItemPaddingRight: 45,
                                                                    tabletItemPaddingLeft: 45,
                                                                    mobileMMenuIconColorPalette: "",
                                                                    mobileMMenuIconColorHex: "#000000",
                                                                    mobileMMenuIconColorOpacity: 1,
                                                                    mobileMMenuItemHorizontalAlign: "center"
                                                                }
                                                            }
                                                        ],
                                                        showOnTablet: "on",
                                                        marginType: "ungrouped"
                                                    }
                                                }
                                            ],
                                            verticalAlign: "center",
                                            width: 40,
                                            tabletWidth: 8.300000000000000710542735760100185871124267578125,
                                            paddingTop: 0,
                                            paddingTopSuffix: "px",
                                            padding: 15,
                                            paddingBottom: 0,
                                            paddingBottomSuffix: "px"
                                        }
                                    },
                                    {
                                        type: "Column",
                                        value: {
                                            _styles: [
                                                "column"
                                            ],
                                            items: [
                                                {
                                                    type: "Wrapper",
                                                    value: {
                                                        _styles: [
                                                            "wrapper",
                                                            "wrapper--image"
                                                        ],
                                                        items: [
                                                            {
                                                                type: "Image",
                                                                value: {
                                                                    _styles: [
                                                                        "image"
                                                                    ],
                                                                    imageWidth: 2129,
                                                                    imageHeight: 457,
                                                                    imageSrc: "b731902c6bf15805972db395c1ec4670.png",
                                                                    height: 100,
                                                                    positionX: 50,
                                                                    positionY: 50,
                                                                    imagePopulation: "",
                                                                    resize: 67,
                                                                    tabletResize: 95,
                                                                    mobileResize: 40
                                                                }
                                                            }
                                                        ],
                                                        horizontalAlign: "center",
                                                        tabletHorizontalAlign: "center",
                                                        mobileHorizontalAlign: "center"
                                                    }
                                                }
                                            ],
                                            width: 22.800000000000000710542735760100185871124267578125,
                                            paddingRight: 0,
                                            paddingRightSuffix: "px",
                                            padding: 15,
                                            paddingLeft: 0,
                                            paddingLeftSuffix: "px",
                                            tabletWidth: 24.89999999999999857891452847979962825775146484375,
                                            verticalAlign: "center",
                                            tabletPaddingLeft: 15,
                                            tabletPaddingLeftSuffix: "px",
                                            tabletPadding: 15
                                        }
                                    },
                                    {
                                        type: "Column",
                                        value: {
                                            _styles: [
                                                "column"
                                            ],
                                            items: [
                                                {
                                                    type: "Wrapper",
                                                    value: {
                                                        _styles: [
                                                            "wrapper",
                                                            "wrapper--menu"
                                                        ],
                                                        items: [
                                                            {
                                                                type: "Menu",
                                                                value: {
                                                                    _styles: [
                                                                        "menu"
                                                                    ],
                                                                    menuSelected: "9d9cfd0e9a8ca3aef9789caa9c69a905",
                                                                    fontSize: 15,
                                                                    fontFamily: "josefin_sans",
                                                                    lineHeight: 1.6999999999999999555910790149937383830547332763671875,
                                                                    letterSpacing: 1,
                                                                    fontWeight: 400,
                                                                    fontStyle: "",
                                                                    itemPadding: 50,
                                                                    itemPaddingRight: 50,
                                                                    itemPaddingLeft: 50,
                                                                    horizontalAlign: "center",
                                                                    colorHex: "#000000",
                                                                    colorOpacity: 1,
                                                                    colorPalette: "color1",
                                                                    tempColorOpacity: 1,
                                                                    hoverColorHex: "#000000",
                                                                    hoverColorOpacity: 1,
                                                                    hoverColorPalette: "color3",
                                                                    subMenuBgColorPalette: "",
                                                                    subMenuBgColorOpacity: 1,
                                                                    subMenuHoverBgColorPalette: "",
                                                                    mMenuBgColorPalette: "",
                                                                    subMenuBorderColorHex: "#000000",
                                                                    subMenuBorderColorOpacity: 0.08000000000000000166533453693773481063544750213623046875,
                                                                    subMenuBorderColorPalette: "",
                                                                    subMenuTempBorderColorOpacity: 0.08000000000000000166533453693773481063544750213623046875,
                                                                    subMenuHoverBorderColorHex: "#000000",
                                                                    subMenuHoverBorderColorOpacity: 0.070000000000000006661338147750939242541790008544921875,
                                                                    mMenuBorderColorHex: "#000000",
                                                                    mMenuBorderColorOpacity: 0.08000000000000000166533453693773481063544750213623046875,
                                                                    subMenuColorHex: "#8b8b8b",
                                                                    subMenuColorOpacity: 1,
                                                                    subMenuColorPalette: "",
                                                                    subMenuTempColorOpacity: 1,
                                                                    subMenuHoverColorHex: "#8b8b8b",
                                                                    subMenuHoverColorOpacity: "#8b8b8b",
                                                                    mMenuColorHex: "#000000",
                                                                    mMenuColorOpacity: 1,
                                                                    mMenuHoverColorHex: "#000000",
                                                                    mMenuHoverColorOpacity: 1,
                                                                    subMenuHoverColorPalette: "color1",
                                                                    mMenuHoverColorPalette: "color3",
                                                                    subMenuHoverBorderColorPalette: "",
                                                                    subMenuTempHoverBorderColorOpacity: 0.070000000000000006661338147750939242541790008544921875,
                                                                    subMenuHoverBgColorHex: "#ffffff",
                                                                    subMenuHoverBgColorOpacity: 1,
                                                                    tabletMMenuSize: 23,
                                                                    tabletHorizontalAlign: "right",
                                                                    tabletMMenuFontSize: 15,
                                                                    tabletMMenuLineHeight: 2,
                                                                    tabletMMenuLetterSpacing: 1,
                                                                    tabletMMenuFontWeight: 400,
                                                                    tabletMMenuFontStyle: "",
                                                                    mMenuBgColorHex: "#ffffff",
                                                                    mMenuBgColorOpacity: 1,
                                                                    mMenuTempBgColorOpacity: 1,
                                                                    mMenuBorderColorPalette: "",
                                                                    mMenuTempBorderColorOpacity: 0.08000000000000000166533453693773481063544750213623046875,
                                                                    mMenuTitle: "Main Menu",
                                                                    mobileHorizontalAlign: "center",
                                                                    mobileMMenuSize: 23,
                                                                    mMenuColorPalette: "color1",
                                                                    mMenuTempColorOpacity: 1,
                                                                    mobileMMenuFontSize: 15,
                                                                    mobileMMenuLineHeight: 2,
                                                                    mobileMMenuLetterSpacing: 1,
                                                                    mobileMMenuFontWeight: 400,
                                                                    mobileMMenuFontStyle: "",
                                                                    tabletMMenuIconColorHex: "",
                                                                    tabletMMenuIconColorOpacity: 1,
                                                                    tabletMMenuIconColorPalette: "color1",
                                                                    mMenu: "off",
                                                                    mMenuSize: 25,
                                                                    subMenuBgColorHex: "#ffffff",
                                                                    subMenuTempBgColorOpacity: 1,
                                                                    subMenuFontSize: 17,
                                                                    subMenuFontFamily: "josefin_sans",
                                                                    subMenuLineHeight: 1,
                                                                    subMenuLetterSpacing: 0,
                                                                    subMenuFontWeight: 400,
                                                                    subMenuFontStyle: "",
                                                                    mMenuIconColorPalette: "color1",
                                                                    mMenuIconColorHex: "",
                                                                    mMenuIconColorOpacity: 1,
                                                                    mMenuFontSize: 15,
                                                                    mMenuFontFamily: "josefin_sans",
                                                                    mMenuLineHeight: 2,
                                                                    mMenuLetterSpacing: 1,
                                                                    mMenuFontWeight: 400,
                                                                    mMenuFontStyle: "",
                                                                    tabletMMenu: "off",
                                                                    tabletFontSize: 15,
                                                                    tabletLineHeight: 1.6999999999999999555910790149937383830547332763671875,
                                                                    tabletLetterSpacing: 1,
                                                                    tabletFontWeight: 400,
                                                                    tabletFontStyle: "",
                                                                    tabletItemPadding: 45,
                                                                    tabletItemPaddingRight: 45,
                                                                    tabletItemPaddingLeft: 45,
                                                                    mobileMMenuIconColorPalette: "",
                                                                    mobileMMenuIconColorHex: "#000000",
                                                                    mobileMMenuIconColorOpacity: 1,
                                                                    mobileMMenuItemHorizontalAlign: "center",
                                                                    mobileMMenu: "off",
                                                                    mobileFontSize: 15,
                                                                    mobileLineHeight: 1.6999999999999999555910790149937383830547332763671875,
                                                                    mobileLetterSpacing: 1,
                                                                    mobileFontWeight: 400,
                                                                    mobileFontStyle: "",
                                                                    mobileItemPadding: 45,
                                                                    mobileItemPaddingRight: 45,
                                                                    mobileItemPaddingLeft: 45,
                                                                    mobileSubMenuFontSize: 13,
                                                                    mobileSubMenuLineHeight: 1.8000000000000000444089209850062616169452667236328125,
                                                                    mobileSubMenuLetterSpacing: 1,
                                                                    mobileSubMenuFontWeight: 600,
                                                                    mobileSubMenuFontStyle: "",
                                                                    tabletSubMenuFontSize: 14,
                                                                    tabletSubMenuLineHeight: 1,
                                                                    tabletSubMenuLetterSpacing: 1,
                                                                    tabletSubMenuFontWeight: 600,
                                                                    tabletSubMenuFontStyle: ""
                                                                }
                                                            }
                                                        ],
                                                        showOnTablet: "on",
                                                        marginType: "ungrouped"
                                                    }
                                                }
                                            ],
                                            verticalAlign: "center",
                                            width: 37.2000000000000028421709430404007434844970703125,
                                            tabletWidth: 66.2999999999999971578290569595992565155029296875,
                                            paddingTop: 0,
                                            paddingTopSuffix: "px",
                                            padding: 15,
                                            paddingBottom: 0,
                                            paddingBottomSuffix: "px"
                                        }
                                    }
                                ],
                                padding: 0,
                                paddingSuffix: "px",
                                paddingTop: 0,
                                paddingRight: 0,
                                paddingBottom: 0,
                                paddingLeft: 0,
                                size: 100,
                                tabletPaddingRight: 0,
                                tabletPaddingRightSuffix: "px",
                                tabletPadding: 0,
                                tabletPaddingLeft: 0,
                                tabletPaddingLeftSuffix: "px"
                            }
                        }
                    ],
                    paddingType: "grouped",
                    paddingTop: 35,
                    paddingBottom: 35,
                    padding: 35,
                    bgColorHex: "#ffffff",
                    bgColorOpacity: 1,
                    bgColorPalette: "",
                    tempBgColorOpacity: 1,
                    tabletPaddingType: "ungrouped",
                    tabletPaddingTop: 15,
                    tabletPaddingBottom: 15,
                    tabletPadding: 50,
                    mobilePaddingType: "ungrouped",
                    mobilePaddingTop: 15,
                    mobilePaddingBottom: 15,
                    mobilePadding: 25,
                    boxShadow: "on",
                    boxShadowBlur: 15,
                    boxShadowColorOpacity: 0.08000000000000000166533453693773481063544750213623046875,
                    boxShadowVertical: 0,
                    boxShadowColorHex: "#000000",
                    boxShadowColorPalette: ""
                }
            },
            {
                type: "SectionHeaderStickyItem",
                value: {
                    _styles: [
                        "sectionHeaderStickyItem"
                    ],
                    items: [],
                    padding: 0,
                    paddingTop: 0,
                    paddingBottom: 0,
                    bgColorHex: "#ffffff",
                    bgColorOpacity: 0.9499999999999999555910790149937383830547332763671875,
                    bgColorPalette: "",
                    tempBgColorOpacity: 0.9499999999999999555910790149937383830547332763671875
                }
            }
        ],
        type: "static"
    }
}
};