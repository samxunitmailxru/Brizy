module.exports = {
id: "Gabi049Dark",

thumbnailWidth: 600,
thumbnailHeight: 309,
title: "Gabi049Dark",

keywords: "Post, Blog",
cat: [17, 18],
type: 1,
pro: true,
resolve: {
    type: "Section",
    value: {
        _styles: [
            "section"
        ],
        items: [
            {
                type: "SectionItem",
                value: {
                    _styles: [
                        "section-item"
                    ],
                    items: [
                        {
                            type: "Row",
                            value: {
                                _styles: [
                                    "row"
                                ],
                                items: [
                                    {
                                        type: "Column",
                                        value: {
                                            _styles: [
                                                "column"
                                            ],
                                            items: [
                                                {
                                                    type: "Wrapper",
                                                    value: {
                                                        _styles: [
                                                            "wrapper",
                                                            "wrapper--richText"
                                                        ],
                                                        items: [
                                                            {
                                                                type: "RichText",
                                                                value: {
                                                                    _styles: [
                                                                        "richText"
                                                                    ],
                                                                    text: "<p class=\"brz-tp-button\"><span style=\"opacity: 0.75;\" class=\"brz-cp-color6\">PHOTOGRAPHY</span></p>"
                                                                }
                                                            }
                                                        ]
                                                    }
                                                },
                                                {
                                                    type: "Wrapper",
                                                    value: {
                                                        _styles: [
                                                            "wrapper",
                                                            "wrapper--richText"
                                                        ],
                                                        items: [
                                                            {
                                                                type: "RichText",
                                                                value: {
                                                                    _styles: [
                                                                        "richText"
                                                                    ],
                                                                    text: "<p class=\"brz-lh-im-1_3 brz-lh-lg-1_3 brz-ff-montserrat brz-fw-lg-700 brz-fw-im-700 brz-fs-lg-62 brz-fs-im-49 brz-ls-lg-m_3_5 brz-ls-im-m_1 brz-text-lg-left\"><span class=\"brz-cp-color8\">Proin Quis Tortor Orci. Etiam At Risus</span></p>"
                                                                }
                                                            }
                                                        ]
                                                    }
                                                },
                                                {
                                                    type: "Row",
                                                    value: {
                                                        _styles: [
                                                            "row",
                                                            "hide-row-borders",
                                                            "padding-0"
                                                        ],
                                                        items: [
                                                            {
                                                                type: "Column",
                                                                value: {
                                                                    _styles: [
                                                                        "column"
                                                                    ],
                                                                    items: [
                                                                        {
                                                                            type: "Wrapper",
                                                                            value: {
                                                                                _styles: [
                                                                                    "wrapper",
                                                                                    "wrapper--iconText"
                                                                                ],
                                                                                items: [
                                                                                    {
                                                                                        type: "IconText",
                                                                                        value: {
                                                                                            _styles: [
                                                                                                "iconText"
                                                                                            ],
                                                                                            items: [
                                                                                                {
                                                                                                    type: "Icon",
                                                                                                    value: {
                                                                                                        _styles: [
                                                                                                            "icon",
                                                                                                            "iconText--icon"
                                                                                                        ],
                                                                                                        name: "chat-round",
                                                                                                        type: "glyph",
                                                                                                        size: "custom",
                                                                                                        customSize: 16,
                                                                                                        borderRadius: 0,
                                                                                                        colorPalette: "color6",
                                                                                                        colorOpacity: 0.5,
                                                                                                        colorHex: "#73777f",
                                                                                                        tempColorOpacity: 0.5,
                                                                                                        hoverColorHex: "#73777f",
                                                                                                        hoverColorOpacity: 0.8000000000000000444089209850062616169452667236328125
                                                                                                    }
                                                                                                },
                                                                                                {
                                                                                                    type: "RichText",
                                                                                                    value: {
                                                                                                        _styles: [
                                                                                                            "text",
                                                                                                            "iconText--text"
                                                                                                        ],
                                                                                                        text: "<p class=\"brz-fs-lg-16 brz-fs-im-15 brz-ls-lg-0 brz-ls-im-0 brz-ff-noto_serif brz-fw-lg-300 brz-fw-im-300 brz-lh-lg-1 brz-lh-im-1\"><span class=\"brz-cp-color6\">14 Commnents&nbsp;</span></p>"
                                                                                                    }
                                                                                                }
                                                                                            ],
                                                                                            iconSpacing: 12
                                                                                        }
                                                                                    }
                                                                                ]
                                                                            }
                                                                        }
                                                                    ],
                                                                    width: 32.5,
                                                                    paddingLeft: 0,
                                                                    paddingLeftSuffix: "px",
                                                                    padding: 15,
                                                                    paddingRight: 0,
                                                                    paddingRightSuffix: "px",
                                                                    mobileMarginBottom: 0,
                                                                    mobileMarginBottomSuffix: "px",
                                                                    mobileMargin: 10
                                                                }
                                                            },
                                                            {
                                                                type: "Column",
                                                                value: {
                                                                    _styles: [
                                                                        "column"
                                                                    ],
                                                                    items: [
                                                                        {
                                                                            type: "Wrapper",
                                                                            value: {
                                                                                _styles: [
                                                                                    "wrapper",
                                                                                    "wrapper--iconText"
                                                                                ],
                                                                                items: [
                                                                                    {
                                                                                        type: "IconText",
                                                                                        value: {
                                                                                            _styles: [
                                                                                                "iconText"
                                                                                            ],
                                                                                            items: [
                                                                                                {
                                                                                                    type: "Icon",
                                                                                                    value: {
                                                                                                        _styles: [
                                                                                                            "icon",
                                                                                                            "iconText--icon"
                                                                                                        ],
                                                                                                        name: "time-3",
                                                                                                        type: "glyph",
                                                                                                        size: "custom",
                                                                                                        customSize: 16,
                                                                                                        borderRadius: 0,
                                                                                                        colorPalette: "color6",
                                                                                                        colorOpacity: 0.5,
                                                                                                        colorHex: "#73777f",
                                                                                                        tempColorOpacity: 0.5,
                                                                                                        hoverColorHex: "#73777f",
                                                                                                        hoverColorOpacity: 0.8000000000000000444089209850062616169452667236328125
                                                                                                    }
                                                                                                },
                                                                                                {
                                                                                                    type: "RichText",
                                                                                                    value: {
                                                                                                        _styles: [
                                                                                                            "text",
                                                                                                            "iconText--text"
                                                                                                        ],
                                                                                                        text: "<p class=\"brz-fs-lg-16 brz-fs-im-15 brz-ls-lg-0 brz-ls-im-0 brz-ff-noto_serif brz-fw-lg-300 brz-fw-im-300 brz-lh-lg-1 brz-lh-im-1\"><span class=\"brz-cp-color6\">August 28, 2018</span></p>"
                                                                                                    }
                                                                                                }
                                                                                            ],
                                                                                            iconSpacing: 12
                                                                                        }
                                                                                    }
                                                                                ]
                                                                            }
                                                                        }
                                                                    ],
                                                                    width: 67.5,
                                                                    paddingLeft: 15,
                                                                    paddingLeftSuffix: "px",
                                                                    padding: 15,
                                                                    mobileMarginBottom: 0,
                                                                    mobileMarginBottomSuffix: "px",
                                                                    mobileMargin: 10
                                                                }
                                                            }
                                                        ]
                                                    }
                                                },
                                                {
                                                    type: "Wrapper",
                                                    value: {
                                                        _styles: [
                                                            "wrapper",
                                                            "wrapper--spacer"
                                                        ],
                                                        items: [
                                                            {
                                                                type: "Spacer",
                                                                value: {
                                                                    _styles: [
                                                                        "spacer"
                                                                    ],
                                                                    height: 15
                                                                }
                                                            }
                                                        ],
                                                        showOnMobile: "off"
                                                    }
                                                },
                                                {
                                                    type: "Wrapper",
                                                    value: {
                                                        _styles: [
                                                            "wrapper",
                                                            "wrapper--line"
                                                        ],
                                                        items: [
                                                            {
                                                                type: "Line",
                                                                value: {
                                                                    _styles: [
                                                                        "line"
                                                                    ],
                                                                    borderColorHex: "#eef0f2",
                                                                    borderColorOpacity: 0.5,
                                                                    borderColorPalette: "color6",
                                                                    width: 13,
                                                                    borderWidth: 2
                                                                }
                                                            }
                                                        ],
                                                        horizontalAlign: "left"
                                                    }
                                                },
                                                {
                                                    type: "Row",
                                                    value: {
                                                        _styles: [
                                                            "row",
                                                            "hide-row-borders",
                                                            "padding-0"
                                                        ],
                                                        items: [
                                                            {
                                                                type: "Column",
                                                                value: {
                                                                    _styles: [
                                                                        "column"
                                                                    ],
                                                                    items: [
                                                                        {
                                                                            type: "Wrapper",
                                                                            value: {
                                                                                _styles: [
                                                                                    "wrapper",
                                                                                    "wrapper--image"
                                                                                ],
                                                                                items: [
                                                                                    {
                                                                                        type: "Image",
                                                                                        value: {
                                                                                            _styles: [
                                                                                                "image"
                                                                                            ],
                                                                                            imageWidth: 700,
                                                                                            imageHeight: 700,
                                                                                            imageSrc: "deffd0af605929f42bc91fe435835a5f.jpg",
                                                                                            height: 100,
                                                                                            positionX: 50,
                                                                                            positionY: 50,
                                                                                            borderRadiusType: "rounded",
                                                                                            borderRadius: 44,
                                                                                            resize: 100,
                                                                                            zoom: 100,
                                                                                            mobileResize: 30
                                                                                        }
                                                                                    }
                                                                                ],
                                                                                horizontalAlign: "left"
                                                                            }
                                                                        }
                                                                    ],
                                                                    width: 10.4000000000000003552713678800500929355621337890625,
                                                                    paddingLeft: 0,
                                                                    paddingLeftSuffix: "px",
                                                                    padding: 15,
                                                                    paddingRight: 0,
                                                                    paddingRightSuffix: "px"
                                                                }
                                                            },
                                                            {
                                                                type: "Column",
                                                                value: {
                                                                    _styles: [
                                                                        "column"
                                                                    ],
                                                                    items: [
                                                                        {
                                                                            type: "Wrapper",
                                                                            value: {
                                                                                _styles: [
                                                                                    "wrapper",
                                                                                    "wrapper--richText"
                                                                                ],
                                                                                items: [
                                                                                    {
                                                                                        type: "RichText",
                                                                                        value: {
                                                                                            _styles: [
                                                                                                "richText"
                                                                                            ],
                                                                                            text: "<p class=\"brz-tp-paragraph\"><span style=\"opacity: 0.8;\" class=\"brz-cp-color6\">Wrriten by&nbsp;</span><strong style=\"opacity: 0.8;\" class=\"brz-cp-color3\"><em>Arabella Ozols</em></strong><strong style=\"opacity: 0.8;\" class=\"brz-cp-color6\"><em>&nbsp;</em></strong></p>"
                                                                                        }
                                                                                    }
                                                                                ]
                                                                            }
                                                                        }
                                                                    ],
                                                                    width: 89.599999999999994315658113919198513031005859375,
                                                                    verticalAlign: "center"
                                                                }
                                                            }
                                                        ]
                                                    }
                                                }
                                            ],
                                            width: 50.39999999999999857891452847979962825775146484375,
                                            verticalAlign: "center",
                                            paddingRight: 15,
                                            paddingRightSuffix: "%",
                                            padding: 15,
                                            paddingLeft: 15,
                                            paddingLeftSuffix: "%",
                                            bgImageWidth: 0,
                                            bgImageHeight: 0,
                                            bgImageSrc: "",
                                            bgPositionX: 50,
                                            bgPositionY: 50,
                                            bgColorOpacity: 0,
                                            tempBgColorOpacity: 0.9499999999999999555910790149937383830547332763671875,
                                            mobilePaddingRight: 0,
                                            mobilePaddingLeft: 0,
                                            bgColorHex: "#ffffff",
                                            bgColorPalette: "",
                                            borderRadius: 0,
                                            borderTopLeftRadius: 0,
                                            borderTopRightRadius: 0,
                                            borderBottomRightRadius: 0,
                                            borderBottomLeftRadius: 0,
                                            paddingTop: 5,
                                            paddingTopSuffix: "%",
                                            paddingBottom: 5,
                                            paddingBottomSuffix: "%",
                                            boxShadow: "off"
                                        }
                                    },
                                    {
                                        type: "Column",
                                        value: {
                                            _styles: [
                                                "column"
                                            ],
                                            items: [
                                                {
                                                    type: "Wrapper",
                                                    value: {
                                                        _styles: [
                                                            "wrapper",
                                                            "wrapper--image"
                                                        ],
                                                        items: [
                                                            {
                                                                type: "Image",
                                                                value: {
                                                                    _styles: [
                                                                        "image"
                                                                    ],
                                                                    imageWidth: 1280,
                                                                    imageHeight: 1920,
                                                                    imageSrc: "42968aadfb7f838f2858874f33e4a245.jpg",
                                                                    height: 51,
                                                                    positionX: 56,
                                                                    positionY: 61,
                                                                    resize: 100,
                                                                    linkType: "lightBox",
                                                                    linkLightBox: "on",
                                                                    boxShadow: "off",
                                                                    boxShadowColorHex: "#000000",
                                                                    boxShadowColorOpacity: 0.200000000000000011102230246251565404236316680908203125,
                                                                    boxShadowColorPalette: "",
                                                                    boxShadowBlur: 23,
                                                                    mobileResize: 96
                                                                }
                                                            }
                                                        ],
                                                        horizontalAlign: "right"
                                                    }
                                                }
                                            ],
                                            width: 49.60000000000000142108547152020037174224853515625,
                                            bgImageWidth: 0,
                                            bgImageHeight: 0,
                                            bgImageSrc: "",
                                            bgPositionX: 50,
                                            bgPositionY: 50,
                                            bgColorOpacity: 0,
                                            tempBgColorOpacity: 1,
                                            mobilePaddingRight: 0,
                                            mobilePaddingLeft: 0,
                                            paddingRight: 0,
                                            paddingRightSuffix: "px",
                                            padding: 15
                                        }
                                    }
                                ],
                                paddingType: "ungrouped",
                                paddingRight: 0,
                                paddingRightSuffix: "px",
                                padding: 10,
                                paddingLeft: 0,
                                paddingLeftSuffix: "px"
                            }
                        }
                    ],
                    containerType: "fullWidth",
                    paddingType: "ungrouped",
                    paddingTop: 75,
                    paddingBottom: 75,
                    padding: 75,
                    bgImageWidth: 0,
                    bgImageHeight: 0,
                    bgImageSrc: "",
                    bgPositionX: 50,
                    bgPositionY: 50,
                    bgColorOpacity: 0.9499999999999999555910790149937383830547332763671875,
                    tempBgColorOpacity: 0.9499999999999999555910790149937383830547332763671875,
                    tempMobileBgColorOpacity: 1,
                    bgColorHex: "#191b21",
                    bgColorPalette: "color1"
                }
            }
        ],
        fullHeight: "off"
    }
}
};