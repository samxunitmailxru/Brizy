module.exports = {
id: "Gabi064Dark",

thumbnailWidth: 600,
thumbnailHeight: 321,
title: "Gabi064Dark",

keywords: "Post, Blog, Content, Gallery, Slider",
cat: [17, 18, 20, 9, 16],
type: 1,
pro: true,
resolve: {
    type: "Section",
    value: {
        _styles: [
            "section"
        ],
        items: [
            {
                type: "SectionItem",
                value: {
                    _styles: [
                        "section-item"
                    ],
                    items: [
                        {
                            type: "Wrapper",
                            value: {
                                _styles: [
                                    "wrapper",
                                    "wrapper--image"
                                ],
                                items: [
                                    {
                                        type: "Image",
                                        value: {
                                            _styles: [
                                                "image"
                                            ],
                                            height: 81,
                                            imageWidth: 1920,
                                            imageHeight: 1280,
                                            imageSrc: "55332f8546b37958743c54de981f1ded.jpg",
                                            positionX: 50,
                                            positionY: 50,
                                            mobileHeight: 100,
                                            boxShadow: "on",
                                            boxShadowColorHex: "#000000",
                                            boxShadowColorOpacity: 0.66000000000000003108624468950438313186168670654296875,
                                            boxShadowColorPalette: "",
                                            boxShadowBlur: 40,
                                            boxShadowSpread: -15,
                                            boxShadowVertical: 26
                                        }
                                    }
                                ]
                            }
                        }
                    ],
                    paddingType: "ungrouped",
                    paddingTop: 90,
                    paddingBottom: 90,
                    padding: 75,
                    bgColorPalette: "color1",
                    bgColorHex: "#191b21",
                    bgColorOpacity: 0.90000000000000002220446049250313080847263336181640625,
                    tempBgColorOpacity: 0.90000000000000002220446049250313080847263336181640625
                }
            },
            {
                type: "SectionItem",
                value: {
                    _styles: [
                        "section-item"
                    ],
                    items: [
                        {
                            type: "Wrapper",
                            value: {
                                _styles: [
                                    "wrapper",
                                    "wrapper--image"
                                ],
                                items: [
                                    {
                                        type: "Image",
                                        value: {
                                            _styles: [
                                                "image"
                                            ],
                                            height: 86,
                                            imageWidth: 1920,
                                            imageHeight: 1200,
                                            imageSrc: "08e5cde7dc728698e21c14f04beeb9cd.jpg",
                                            positionX: 50,
                                            positionY: 50,
                                            mobileHeight: 100,
                                            boxShadow: "on",
                                            boxShadowBlur: 40,
                                            boxShadowColorOpacity: 0.65000000000000002220446049250313080847263336181640625,
                                            boxShadowSpread: -15,
                                            boxShadowVertical: 25,
                                            boxShadowColorHex: "#000000",
                                            boxShadowColorPalette: "",
                                            boxShadowHorizontal: 0
                                        }
                                    }
                                ]
                            }
                        }
                    ],
                    paddingType: "ungrouped",
                    paddingTop: 15,
                    paddingBottom: 35,
                    padding: 75,
                    bgColorHex: "#191b21",
                    bgColorOpacity: 0.90000000000000002220446049250313080847263336181640625,
                    bgColorPalette: "color1",
                    tempBgColorOpacity: 0.90000000000000002220446049250313080847263336181640625
                }
            },
            {
                type: "SectionItem",
                value: {
                    _styles: [
                        "section-item"
                    ],
                    items: [
                        {
                            type: "Wrapper",
                            value: {
                                _styles: [
                                    "wrapper",
                                    "wrapper--image"
                                ],
                                items: [
                                    {
                                        type: "Image",
                                        value: {
                                            _styles: [
                                                "image"
                                            ],
                                            height: 86,
                                            imageWidth: 1920,
                                            imageHeight: 1200,
                                            imageSrc: "a0db0f3f0405eff3f09e3e9142d16656.jpg",
                                            positionX: 50,
                                            positionY: 50,
                                            mobileHeight: 100,
                                            boxShadow: "on",
                                            boxShadowColorHex: "#000000",
                                            boxShadowColorOpacity: 0.65000000000000002220446049250313080847263336181640625,
                                            boxShadowColorPalette: "",
                                            boxShadowSpread: -15,
                                            boxShadowBlur: 40,
                                            boxShadowVertical: 23,
                                            boxShadowHorizontal: 0
                                        }
                                    }
                                ]
                            }
                        }
                    ],
                    paddingType: "ungrouped",
                    paddingTop: 60,
                    paddingBottom: 60,
                    padding: 75,
                    bgColorPalette: "color1",
                    bgColorHex: "#191b21",
                    bgColorOpacity: 0.90000000000000002220446049250313080847263336181640625,
                    tempBgColorOpacity: 0.90000000000000002220446049250313080847263336181640625
                }
            }
        ],
        slider: "on",
        sliderArrowsColorPalette: "color4",
        sliderArrowsColorOpacity: 1,
        sliderArrowsColorHex: "#66738d",
        sliderDotsColorHex: "#66738d",
        sliderDotsColorOpacity: 1,
        sliderDotsColorPalette: "color4",
        sliderDots: "circle",
        sliderArrows: "tail",
        sliderAutoPlay: "on"
    }
}
};