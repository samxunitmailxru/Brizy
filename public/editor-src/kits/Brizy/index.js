const Blank000Dark = require("./blocks/Blank000Dark");
const Blank000Light = require("./blocks/Blank000Light");
const Bogdan001Dark = require("./blocks/Bogdan001Dark");
const Bogdan001Light = require("./blocks/Bogdan001Light");
const Dimi001Dark = require("./blocks/Dimi001Dark");
const Dimi001Light = require("./blocks/Dimi001Light");
const Gabi001Dark = require("./blocks/Gabi001Dark");
const Gabi001Light = require("./blocks/Gabi001Light");
const Bogdan002Dark = require("./blocks/Bogdan002Dark");
const Bogdan002Light = require("./blocks/Bogdan002Light");
const Dimi002Dark = require("./blocks/Dimi002Dark");
const Dimi002Light = require("./blocks/Dimi002Light");
const Gabi002Dark = require("./blocks/Gabi002Dark");
const Gabi002Light = require("./blocks/Gabi002Light");
const Bogdan003Dark = require("./blocks/Bogdan003Dark");
const Bogdan003Light = require("./blocks/Bogdan003Light");
const Dimi003Dark = require("./blocks/Dimi003Dark");
const Dimi003Light = require("./blocks/Dimi003Light");
const Gabi003Dark = require("./blocks/Gabi003Dark");
const Gabi003Light = require("./blocks/Gabi003Light");
const Bogdan004Dark = require("./blocks/Bogdan004Dark");
const Bogdan004Light = require("./blocks/Bogdan004Light");
const Dimi004Dark = require("./blocks/Dimi004Dark");
const Dimi004Light = require("./blocks/Dimi004Light");
const Gabi004Dark = require("./blocks/Gabi004Dark");
const Gabi004Light = require("./blocks/Gabi004Light");
const Bogdan005Dark = require("./blocks/Bogdan005Dark");
const Bogdan005Light = require("./blocks/Bogdan005Light");
const Dimi005Dark = require("./blocks/Dimi005Dark");
const Dimi005Light = require("./blocks/Dimi005Light");
const Gabi005Dark = require("./blocks/Gabi005Dark");
const Gabi005Light = require("./blocks/Gabi005Light");
const Bogdan006Dark = require("./blocks/Bogdan006Dark");
const Bogdan006Light = require("./blocks/Bogdan006Light");
const Dimi006Dark = require("./blocks/Dimi006Dark");
const Dimi006Light = require("./blocks/Dimi006Light");
const Gabi006Dark = require("./blocks/Gabi006Dark");
const Gabi006Light = require("./blocks/Gabi006Light");
const Bogdan007Dark = require("./blocks/Bogdan007Dark");
const Bogdan007Light = require("./blocks/Bogdan007Light");
const Dimi007Dark = require("./blocks/Dimi007Dark");
const Dimi007Light = require("./blocks/Dimi007Light");
const Gabi007Dark = require("./blocks/Gabi007Dark");
const Gabi007Light = require("./blocks/Gabi007Light");
const Bogdan008Dark = require("./blocks/Bogdan008Dark");
const Bogdan008Light = require("./blocks/Bogdan008Light");
const Dimi008Dark = require("./blocks/Dimi008Dark");
const Dimi008Light = require("./blocks/Dimi008Light");
const Gabi008Dark = require("./blocks/Gabi008Dark");
const Gabi008Light = require("./blocks/Gabi008Light");
const Bogdan009Dark = require("./blocks/Bogdan009Dark");
const Bogdan009Light = require("./blocks/Bogdan009Light");
const Dimi009Dark = require("./blocks/Dimi009Dark");
const Dimi009Light = require("./blocks/Dimi009Light");
const Gabi009Dark = require("./blocks/Gabi009Dark");
const Gabi009Light = require("./blocks/Gabi009Light");
const Bogdan010Dark = require("./blocks/Bogdan010Dark");
const Bogdan010Light = require("./blocks/Bogdan010Light");
const Dimi010Dark = require("./blocks/Dimi010Dark");
const Dimi010Light = require("./blocks/Dimi010Light");
const Gabi010Dark = require("./blocks/Gabi010Dark");
const Gabi010Light = require("./blocks/Gabi010Light");
const Bogdan011Dark = require("./blocks/Bogdan011Dark");
const Bogdan011Light = require("./blocks/Bogdan011Light");
const Dimi011Dark = require("./blocks/Dimi011Dark");
const Dimi011Light = require("./blocks/Dimi011Light");
const Gabi011Dark = require("./blocks/Gabi011Dark");
const Gabi011Light = require("./blocks/Gabi011Light");
const Bogdan012Dark = require("./blocks/Bogdan012Dark");
const Bogdan012Light = require("./blocks/Bogdan012Light");
const Dimi012Dark = require("./blocks/Dimi012Dark");
const Dimi012Light = require("./blocks/Dimi012Light");
const Gabi012Dark = require("./blocks/Gabi012Dark");
const Gabi012Light = require("./blocks/Gabi012Light");
const Dimi013Dark = require("./blocks/Dimi013Dark");
const Dimi013Light = require("./blocks/Dimi013Light");
const Gabi013Dark = require("./blocks/Gabi013Dark");
const Gabi013Light = require("./blocks/Gabi013Light");
const Dimi014Dark = require("./blocks/Dimi014Dark");
const Dimi014Light = require("./blocks/Dimi014Light");
const Gabi014Dark = require("./blocks/Gabi014Dark");
const Gabi014Light = require("./blocks/Gabi014Light");
const Bogdan015Dark = require("./blocks/Bogdan015Dark");
const Bogdan015Light = require("./blocks/Bogdan015Light");
const Dimi015Dark = require("./blocks/Dimi015Dark");
const Dimi015Light = require("./blocks/Dimi015Light");
const Gabi015Dark = require("./blocks/Gabi015Dark");
const Gabi015Light = require("./blocks/Gabi015Light");
const Bogdan016Dark = require("./blocks/Bogdan016Dark");
const Bogdan016Light = require("./blocks/Bogdan016Light");
const Dimi016Dark = require("./blocks/Dimi016Dark");
const Dimi016Light = require("./blocks/Dimi016Light");
const Gabi016Dark = require("./blocks/Gabi016Dark");
const Gabi016Light = require("./blocks/Gabi016Light");
const Bogdan017Dark = require("./blocks/Bogdan017Dark");
const Bogdan017Light = require("./blocks/Bogdan017Light");
const Dimi017Dark = require("./blocks/Dimi017Dark");
const Dimi017Light = require("./blocks/Dimi017Light");
const Gabi017Dark = require("./blocks/Gabi017Dark");
const Gabi017Light = require("./blocks/Gabi017Light");
const Bogdan018Dark = require("./blocks/Bogdan018Dark");
const Bogdan018Light = require("./blocks/Bogdan018Light");
const Dimi018Dark = require("./blocks/Dimi018Dark");
const Dimi018Light = require("./blocks/Dimi018Light");
const Gabi018Dark = require("./blocks/Gabi018Dark");
const Gabi018Light = require("./blocks/Gabi018Light");
const Bogdan019Dark = require("./blocks/Bogdan019Dark");
const Bogdan019Light = require("./blocks/Bogdan019Light");
const Dimi019Dark = require("./blocks/Dimi019Dark");
const Dimi019Light = require("./blocks/Dimi019Light");
const Gabi019Dark = require("./blocks/Gabi019Dark");
const Gabi019Light = require("./blocks/Gabi019Light");
const Bogdan020Dark = require("./blocks/Bogdan020Dark");
const Bogdan020Light = require("./blocks/Bogdan020Light");
const Gabi020Dark = require("./blocks/Gabi020Dark");
const Gabi020Light = require("./blocks/Gabi020Light");
const Bogdan021Dark = require("./blocks/Bogdan021Dark");
const Bogdan021Light = require("./blocks/Bogdan021Light");
const Gabi021Dark = require("./blocks/Gabi021Dark");
const Gabi021Light = require("./blocks/Gabi021Light");
const Bogdan022Dark = require("./blocks/Bogdan022Dark");
const Bogdan022Light = require("./blocks/Bogdan022Light");
const Gabi022Dark = require("./blocks/Gabi022Dark");
const Gabi022Light = require("./blocks/Gabi022Light");
const Bogdan023Dark = require("./blocks/Bogdan023Dark");
const Bogdan023Light = require("./blocks/Bogdan023Light");
const Gabi023Dark = require("./blocks/Gabi023Dark");
const Gabi023Light = require("./blocks/Gabi023Light");
const Bogdan024Dark = require("./blocks/Bogdan024Dark");
const Bogdan024Light = require("./blocks/Bogdan024Light");
const Gabi024Dark = require("./blocks/Gabi024Dark");
const Gabi024Light = require("./blocks/Gabi024Light");
const Gabi025Dark = require("./blocks/Gabi025Dark");
const Gabi025Light = require("./blocks/Gabi025Light");
const Gabi026Dark = require("./blocks/Gabi026Dark");
const Gabi026Light = require("./blocks/Gabi026Light");
const Gabi027Dark = require("./blocks/Gabi027Dark");
const Gabi027Light = require("./blocks/Gabi027Light");
const Gabi028Dark = require("./blocks/Gabi028Dark");
const Gabi028Light = require("./blocks/Gabi028Light");
const Gabi029Dark = require("./blocks/Gabi029Dark");
const Gabi029Light = require("./blocks/Gabi029Light");
const Gabi030Dark = require("./blocks/Gabi030Dark");
const Gabi030Light = require("./blocks/Gabi030Light");
const Gabi031Dark = require("./blocks/Gabi031Dark");
const Gabi031Light = require("./blocks/Gabi031Light");
const Gabi032Dark = require("./blocks/Gabi032Dark");
const Gabi032Light = require("./blocks/Gabi032Light");
const Gabi033Dark = require("./blocks/Gabi033Dark");
const Gabi033Light = require("./blocks/Gabi033Light");
const Gabi034Dark = require("./blocks/Gabi034Dark");
const Gabi034Light = require("./blocks/Gabi034Light");
const Gabi035Dark = require("./blocks/Gabi035Dark");
const Gabi035Light = require("./blocks/Gabi035Light");
const Gabi036Dark = require("./blocks/Gabi036Dark");
const Gabi036Light = require("./blocks/Gabi036Light");
const Gabi037Dark = require("./blocks/Gabi037Dark");
const Gabi037Light = require("./blocks/Gabi037Light");
const Gabi038Dark = require("./blocks/Gabi038Dark");
const Gabi038Light = require("./blocks/Gabi038Light");
const Gabi039Dark = require("./blocks/Gabi039Dark");
const Gabi039Light = require("./blocks/Gabi039Light");
const Gabi040Dark = require("./blocks/Gabi040Dark");
const Gabi040Light = require("./blocks/Gabi040Light");
const Gabi041Dark = require("./blocks/Gabi041Dark");
const Gabi041Light = require("./blocks/Gabi041Light");
const Gabi042Dark = require("./blocks/Gabi042Dark");
const Gabi042Light = require("./blocks/Gabi042Light");
const Gabi043Dark = require("./blocks/Gabi043Dark");
const Gabi043Light = require("./blocks/Gabi043Light");
const Gabi044Dark = require("./blocks/Gabi044Dark");
const Gabi044Light = require("./blocks/Gabi044Light");
const Gabi045Dark = require("./blocks/Gabi045Dark");
const Gabi045Light = require("./blocks/Gabi045Light");
const Gabi046Dark = require("./blocks/Gabi046Dark");
const Gabi046Light = require("./blocks/Gabi046Light");
const Gabi047Dark = require("./blocks/Gabi047Dark");
const Gabi047Light = require("./blocks/Gabi047Light");
const Gabi049Dark = require("./blocks/Gabi049Dark");
const Gabi049Light = require("./blocks/Gabi049Light");
const Gabi050Dark = require("./blocks/Gabi050Dark");
const Gabi050Light = require("./blocks/Gabi050Light");
const Gabi051Dark = require("./blocks/Gabi051Dark");
const Gabi051Light = require("./blocks/Gabi051Light");
const Gabi052Dark = require("./blocks/Gabi052Dark");
const Gabi052Light = require("./blocks/Gabi052Light");
const Gabi053Dark = require("./blocks/Gabi053Dark");
const Gabi053Light = require("./blocks/Gabi053Light");
const Gabi054Dark = require("./blocks/Gabi054Dark");
const Gabi054Light = require("./blocks/Gabi054Light");
const Gabi055Dark = require("./blocks/Gabi055Dark");
const Gabi055Light = require("./blocks/Gabi055Light");
const Gabi056Dark = require("./blocks/Gabi056Dark");
const Gabi056Light = require("./blocks/Gabi056Light");
const Gabi057Dark = require("./blocks/Gabi057Dark");
const Gabi057Light = require("./blocks/Gabi057Light");
const Gabi058Dark = require("./blocks/Gabi058Dark");
const Gabi058Light = require("./blocks/Gabi058Light");
const Gabi059Dark = require("./blocks/Gabi059Dark");
const Gabi059Light = require("./blocks/Gabi059Light");
const Gabi060Dark = require("./blocks/Gabi060Dark");
const Gabi060Light = require("./blocks/Gabi060Light");
const Gabi061Dark = require("./blocks/Gabi061Dark");
const Gabi061Light = require("./blocks/Gabi061Light");
const Gabi062Dark = require("./blocks/Gabi062Dark");
const Gabi062Light = require("./blocks/Gabi062Light");
const Gabi063Dark = require("./blocks/Gabi063Dark");
const Gabi063Light = require("./blocks/Gabi063Light");
const Gabi064Dark = require("./blocks/Gabi064Dark");
const Gabi064Light = require("./blocks/Gabi064Light");
const Gabi065Dark = require("./blocks/Gabi065Dark");
const Gabi065Light = require("./blocks/Gabi065Light");
const Gabi066Dark = require("./blocks/Gabi066Dark");
const Gabi066Light = require("./blocks/Gabi066Light");
const Gabi067Dark = require("./blocks/Gabi067Dark");
const Gabi067Light = require("./blocks/Gabi067Light");
const Gabi068Dark = require("./blocks/Gabi068Dark");
const Gabi068Light = require("./blocks/Gabi068Light");
const Gabi069Dark = require("./blocks/Gabi069Dark");
const Gabi069Light = require("./blocks/Gabi069Light");
const Gabi070Dark = require("./blocks/Gabi070Dark");
const Gabi070Light = require("./blocks/Gabi070Light");
const Gabi071Dark = require("./blocks/Gabi071Dark");
const Gabi071Light = require("./blocks/Gabi071Light");
const Gabi072Dark = require("./blocks/Gabi072Dark");
const Gabi072Light = require("./blocks/Gabi072Light");
const Gabi073Dark = require("./blocks/Gabi073Dark");
const Gabi073Light = require("./blocks/Gabi073Light");
const Gabi074Dark = require("./blocks/Gabi074Dark");
const Gabi074Light = require("./blocks/Gabi074Light");
const Gabi075Dark = require("./blocks/Gabi075Dark");
const Gabi075Light = require("./blocks/Gabi075Light");
const Gabi076Dark = require("./blocks/Gabi076Dark");
const Gabi076Light = require("./blocks/Gabi076Light");
const Dimi077Dark = require("./blocks/Dimi077Dark");
const Dimi077Light = require("./blocks/Dimi077Light");
const Dimi078Dark = require("./blocks/Dimi078Dark");
const Dimi078Light = require("./blocks/Dimi078Light");
const Dimi079Dark = require("./blocks/Dimi079Dark");
const Dimi079Light = require("./blocks/Dimi079Light");
const Dimi080Dark = require("./blocks/Dimi080Dark");
const Dimi080Light = require("./blocks/Dimi080Light");
const Dimi081Dark = require("./blocks/Dimi081Dark");
const Dimi081Light = require("./blocks/Dimi081Light");
const Dimi082Dark = require("./blocks/Dimi082Dark");
const Dimi082Light = require("./blocks/Dimi082Light");
const Dimi083Dark = require("./blocks/Dimi083Dark");
const Dimi083Light = require("./blocks/Dimi083Light");
const Dimi084Dark = require("./blocks/Dimi084Dark");
const Dimi084Light = require("./blocks/Dimi084Light");
const Dimi085Dark = require("./blocks/Dimi085Dark");
const Dimi085Light = require("./blocks/Dimi085Light");
const Dimi086Dark = require("./blocks/Dimi086Dark");
const Dimi086Light = require("./blocks/Dimi086Light");
const Dimi087Dark = require("./blocks/Dimi087Dark");
const Dimi087Light = require("./blocks/Dimi087Light");
const Dimi088Dark = require("./blocks/Dimi088Dark");
const Dimi088Light = require("./blocks/Dimi088Light");
const Dimi089Dark = require("./blocks/Dimi089Dark");
const Dimi089Light = require("./blocks/Dimi089Light");
const Dimi090Dark = require("./blocks/Dimi090Dark");
const Dimi090Light = require("./blocks/Dimi090Light");
const Dimi091Dark = require("./blocks/Dimi091Dark");
const Dimi091Light = require("./blocks/Dimi091Light");
const Dimi092Dark = require("./blocks/Dimi092Dark");
const Dimi092Light = require("./blocks/Dimi092Light");
const Dimi093Dark = require("./blocks/Dimi093Dark");
const Dimi093Light = require("./blocks/Dimi093Light");
const BrizyDefault = require("./styles/default");

module.exports = {
  id: "kxnmljnsniklnqftyvrkkzwhcvudtcmxikok",
  name: "Kit #1",
  blocks: [
    Blank000Dark,
    Blank000Light,
    Bogdan001Dark,
    Bogdan001Light,
    Dimi001Dark,
    Dimi001Light,
    Gabi001Dark,
    Gabi001Light,
    Bogdan002Dark,
    Bogdan002Light,
    Dimi002Dark,
    Dimi002Light,
    Gabi002Dark,
    Gabi002Light,
    Bogdan003Dark,
    Bogdan003Light,
    Dimi003Dark,
    Dimi003Light,
    Gabi003Dark,
    Gabi003Light,
    Bogdan004Dark,
    Bogdan004Light,
    Dimi004Dark,
    Dimi004Light,
    Gabi004Dark,
    Gabi004Light,
    Bogdan005Dark,
    Bogdan005Light,
    Dimi005Dark,
    Dimi005Light,
    Gabi005Dark,
    Gabi005Light,
    Bogdan006Dark,
    Bogdan006Light,
    Dimi006Dark,
    Dimi006Light,
    Gabi006Dark,
    Gabi006Light,
    Bogdan007Dark,
    Bogdan007Light,
    Dimi007Dark,
    Dimi007Light,
    Gabi007Dark,
    Gabi007Light,
    Bogdan008Dark,
    Bogdan008Light,
    Dimi008Dark,
    Dimi008Light,
    Gabi008Dark,
    Gabi008Light,
    Bogdan009Dark,
    Bogdan009Light,
    Dimi009Dark,
    Dimi009Light,
    Gabi009Dark,
    Gabi009Light,
    Bogdan010Dark,
    Bogdan010Light,
    Dimi010Dark,
    Dimi010Light,
    Gabi010Dark,
    Gabi010Light,
    Bogdan011Dark,
    Bogdan011Light,
    Dimi011Dark,
    Dimi011Light,
    Gabi011Dark,
    Gabi011Light,
    Bogdan012Dark,
    Bogdan012Light,
    Dimi012Dark,
    Dimi012Light,
    Gabi012Dark,
    Gabi012Light,
    Dimi013Dark,
    Dimi013Light,
    Gabi013Dark,
    Gabi013Light,
    Dimi014Dark,
    Dimi014Light,
    Gabi014Dark,
    Gabi014Light,
    Bogdan015Dark,
    Bogdan015Light,
    Dimi015Dark,
    Dimi015Light,
    Gabi015Dark,
    Gabi015Light,
    Bogdan016Dark,
    Bogdan016Light,
    Dimi016Dark,
    Dimi016Light,
    Gabi016Dark,
    Gabi016Light,
    Bogdan017Dark,
    Bogdan017Light,
    Dimi017Dark,
    Dimi017Light,
    Gabi017Dark,
    Gabi017Light,
    Bogdan018Dark,
    Bogdan018Light,
    Dimi018Dark,
    Dimi018Light,
    Gabi018Dark,
    Gabi018Light,
    Bogdan019Dark,
    Bogdan019Light,
    Dimi019Dark,
    Dimi019Light,
    Gabi019Dark,
    Gabi019Light,
    Bogdan020Dark,
    Bogdan020Light,
    Gabi020Dark,
    Gabi020Light,
    Bogdan021Dark,
    Bogdan021Light,
    Gabi021Dark,
    Gabi021Light,
    Bogdan022Dark,
    Bogdan022Light,
    Gabi022Dark,
    Gabi022Light,
    Bogdan023Dark,
    Bogdan023Light,
    Gabi023Dark,
    Gabi023Light,
    Bogdan024Dark,
    Bogdan024Light,
    Gabi024Dark,
    Gabi024Light,
    Gabi025Dark,
    Gabi025Light,
    Gabi026Dark,
    Gabi026Light,
    Gabi027Dark,
    Gabi027Light,
    Gabi028Dark,
    Gabi028Light,
    Gabi029Dark,
    Gabi029Light,
    Gabi030Dark,
    Gabi030Light,
    Gabi031Dark,
    Gabi031Light,
    Gabi032Dark,
    Gabi032Light,
    Gabi033Dark,
    Gabi033Light,
    Gabi034Dark,
    Gabi034Light,
    Gabi035Dark,
    Gabi035Light,
    Gabi036Dark,
    Gabi036Light,
    Gabi037Dark,
    Gabi037Light,
    Gabi038Dark,
    Gabi038Light,
    Gabi039Dark,
    Gabi039Light,
    Gabi040Dark,
    Gabi040Light,
    Gabi041Dark,
    Gabi041Light,
    Gabi042Dark,
    Gabi042Light,
    Gabi043Dark,
    Gabi043Light,
    Gabi044Dark,
    Gabi044Light,
    Gabi045Dark,
    Gabi045Light,
    Gabi046Dark,
    Gabi046Light,
    Gabi047Dark,
    Gabi047Light,
    Gabi049Dark,
    Gabi049Light,
    Gabi050Dark,
    Gabi050Light,
    Gabi051Dark,
    Gabi051Light,
    Gabi052Dark,
    Gabi052Light,
    Gabi053Dark,
    Gabi053Light,
    Gabi054Dark,
    Gabi054Light,
    Gabi055Dark,
    Gabi055Light,
    Gabi056Dark,
    Gabi056Light,
    Gabi057Dark,
    Gabi057Light,
    Gabi058Dark,
    Gabi058Light,
    Gabi059Dark,
    Gabi059Light,
    Gabi060Dark,
    Gabi060Light,
    Gabi061Dark,
    Gabi061Light,
    Gabi062Dark,
    Gabi062Light,
    Gabi063Dark,
    Gabi063Light,
    Gabi064Dark,
    Gabi064Light,
    Gabi065Dark,
    Gabi065Light,
    Gabi066Dark,
    Gabi066Light,
    Gabi067Dark,
    Gabi067Light,
    Gabi068Dark,
    Gabi068Light,
    Gabi069Dark,
    Gabi069Light,
    Gabi070Dark,
    Gabi070Light,
    Gabi071Dark,
    Gabi071Light,
    Gabi072Dark,
    Gabi072Light,
    Gabi073Dark,
    Gabi073Light,
    Gabi074Dark,
    Gabi074Light,
    Gabi075Dark,
    Gabi075Light,
    Gabi076Dark,
    Gabi076Light,
    Dimi077Dark,
    Dimi077Light,
    Dimi078Dark,
    Dimi078Light,
    Dimi079Dark,
    Dimi079Light,
    Dimi080Dark,
    Dimi080Light,
    Dimi081Dark,
    Dimi081Light,
    Dimi082Dark,
    Dimi082Light,
    Dimi083Dark,
    Dimi083Light,
    Dimi084Dark,
    Dimi084Light,
    Dimi085Dark,
    Dimi085Light,
    Dimi086Dark,
    Dimi086Light,
    Dimi087Dark,
    Dimi087Light,
    Dimi088Dark,
    Dimi088Light,
    Dimi089Dark,
    Dimi089Light,
    Dimi090Dark,
    Dimi090Light,
    Dimi091Dark,
    Dimi091Light,
    Dimi092Dark,
    Dimi092Light,
    Dimi093Dark,
    Dimi093Light
  ],
  styles: [BrizyDefault],
  types: [
    {
      id: 0,
      name: "light",
      title: "Light",
      icon: "nc-light"
    },
    {
      id: 1,
      name: "dark",
      title: "Dark",
      icon: "nc-dark"
    }
  ],
  categories: [
    { id: 0, slug: "blank", title: "Blank", hidden: true },
    { id: 2, slug: "cover", title: "Cover" },
    { id: 3, slug: "features", title: "Features" },
    { id: 4, slug: "call-to-action", title: "Call to Action" },
    { id: 5, slug: "projects", title: "Projects" },
    { id: 6, slug: "news", title: "News" },
    { id: 7, slug: "services", title: "Services" },
    { id: 8, slug: "testimonial", title: "Testimonial" },
    { id: 9, slug: "gallery", title: "Gallery" },
    { id: 10, slug: "team", title: "Team" },
    { id: 11, slug: "contact", title: "Contact" },
    { id: 12, slug: "pricing", title: "Pricing" },
    { id: 13, slug: "social", title: "Social" },
    { id: 14, slug: "map", title: "Map" },
    { id: 15, slug: "forms", title: "Forms" },
    { id: 16, slug: "slider", title: "Slider" },
    { id: 17, slug: "post", title: "Post" },
    { id: 18, slug: "blog", title: "Blog" },
    { id: 19, slug: "author", title: "Author" },
    { id: 20, slug: "content", title: "Content" },
    { id: 21, slug: "header", title: "Header" },
    { id: 22, slug: "footer", title: "Footer" }
  ]
};
